from .event_views import EventList
from .general_views import (Custom404, DataProtectionDeclaration, GeneralTerms,
                            LegalNotice)
from .registration_views import RegistrationCreate, RegistrationSignOff
from .waitlistentry_views import (WaitListEntryConfirm, WaitListEntryCreate,
                                  WaitListSignOff)

__all__ = [
    EventList, Custom404, DataProtectionDeclaration, GeneralTerms, LegalNotice,
    RegistrationCreate, RegistrationSignOff, WaitListEntryConfirm,
    WaitListEntryCreate, WaitListSignOff,
]
