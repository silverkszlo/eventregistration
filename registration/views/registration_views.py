from smtplib import SMTPRecipientsRefused

from django import forms
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import CreateView, UpdateView

from registration.mixins import EventMixin
from registration.models import Event, Registration, WaitListEntry
from registration.utils.mails import (send_email_already_used_mail,
                                      send_final_confirmation_mail,
                                      send_organizer_sign_off_mail)


class RegistrationCreate(EventMixin, CreateView):
    class RegistrationCreateForm(forms.ModelForm):
        class Meta:
            model = Registration
            fields = [
                'participant_name',
                'email_address',
                'comment',
                'answer',
            ]

        def __init__(self, *args, **kwargs):
            event = kwargs.pop('event')
            super().__init__(*args, **kwargs)
            if event.low_fee:
                self.fields['participation_fee'] = forms.ChoiceField()
                choices = [
                    ('', '---'),  # initial empty value
                    (event.low_fee, _("Reduced") + " - " + str(event.low_fee) + "€"),
                    (event.medium_fee, _("Break-even") + " - " + str(event.medium_fee) + "€"),
                    (event.high_fee, _("Supporter") + " - " + str(event.high_fee) + "€"),
                ]
                self.fields['participation_fee'].choices = choices
                self.fields['participation_fee'].label = _('Participation fee')

        def save(self, commit=True):
            instance = super().save(commit=False)
            instance.participation_fee = self.cleaned_data.get('participation_fee')
            if commit:
                instance.save()
            return instance

    model = Registration
    template_name = 'registration/registration-create.html'
    context_object_name = 'registration'
    allowed_status = [Event.OPEN, Event.WAITLIST, Event.CLOSED]
    pk_url_kwarg = 'waitlist_pk'
    form_class = RegistrationCreateForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['event'] = self.waitlistentry.event
        return kwargs

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.waitlistentry = get_object_or_404(
            WaitListEntry, uuid=self.kwargs.get('waitlist_pk'), confirmed=True,
        )

    def dispatch(self, request, *args, **kwargs):
        if self.waitlistentry.event.status == Event.CLOSED:
            messages.error(
                self.request,
                _("The registration for this event has already ended.")
            )
            return redirect('event-list')

        elif (
            self.waitlistentry.event.remaining_capacity_only_registrations <= 0
            or (self.waitlistentry.event.status != Event.OPEN and self.waitlistentry.reminded_at is None)
        ):
            messages.error(
                self.request,
                _(
                    "All seats for this event are taken. "
                    "You will be notified via email when the next person signs off."
                )
            )
            self.waitlistentry.reminded_at = None
            self.waitlistentry.save()
            return redirect('event-list')

        # check if this waitlistentry has already been used to sign up for this event
        elif Registration.objects.filter(
            event=self.event,
            uuid=self.waitlistentry.uuid,
        ).exists():
            messages.error(
                self.request,
                _(
                    "Your spot on the waitlist has already been used to sign up for this event."
                    "Please confirm your registration. You can find the link in your emails."
                )
            )
            return redirect('event-list')

        else:
            return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        registration = form.save(commit=False)
        registration.event = self.waitlistentry.event
        registration.uuid = self.waitlistentry.uuid

        # prevent leaking the email address by always displaying success message but
        # privately informing if email has already been used
        try:
            if registration.email_already_used:
                send_email_already_used_mail(registration.email_already_used)
            else:
                registration.save()
                send_final_confirmation_mail(registration)

        except SMTPRecipientsRefused:
            form.add_error('email_address', _("Please enter a valid email address."))
            return super().form_invalid(form)

        messages.success(self.request, _("Thank you for registering for our event!"))
        self.waitlistentry.delete()
        return redirect('event-list')

    def get_initial(self):
        initial = super().get_initial()
        initial['participant_name'] = self.waitlistentry.participant_name
        initial['email_address'] = self.waitlistentry.email_address
        return initial


class RegistrationSignOff(EventMixin, UpdateView):
    model = Registration
    fields = ['event']
    template_name = 'registration/registration-sign-off.html'
    success_url = reverse_lazy('event-list')
    context_object_name = 'registration'
    pk_url_kwarg = 'registration_pk'
    allowed_status = [Event.OPEN, Event.WAITLIST, Event.CLOSED]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['registration'] = get_object_or_404(Registration, uuid=self.kwargs.get('registration_pk'))
        return context

    def dispatch(self, request, *args, **kwargs):
        self.object = get_object_or_404(Registration, uuid=self.kwargs.get('registration_pk'))
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        registration = get_object_or_404(Registration, uuid=self.kwargs.get('registration_pk'))

        if not registration.event.status == Event.CLOSED and registration.event.free_cancellation_period:
            registration.delete()
        else:
            registration.signed_off = True
            registration.save()
        messages.success(
            self.request,
            _("You have successfully signed off.")
        )

        # this needs to come after the actual sign off in order for
        # remaining_capacity_only_registrations to be correct
        if (
            self.event.status == Event.CLOSED
            and self.event.remaining_capacity_only_registrations > 0
            and self.event.waiting > 0
        ):
            send_organizer_sign_off_mail(self.event)

        return redirect('event-list')
