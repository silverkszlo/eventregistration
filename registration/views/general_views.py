from django.shortcuts import render
from django.views.generic import TemplateView


class GeneralTerms(TemplateView):
    template_name = 'registration/general-terms.html'


class DataProtectionDeclaration(TemplateView):
    template_name = 'registration/data-protection-declaration.html'


class LegalNotice(TemplateView):
    template_name = 'registration/legal-notice.html'


class Custom404(TemplateView):
    template_name = 'registration/404.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, status=404)
