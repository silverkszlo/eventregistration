
from django.views.generic import ListView

from registration.models import Event


class EventList(ListView):
    model = Event
    template_name = 'registration/event-list.html'
    context_object_name = 'events'
    # not used for EventMixin in this case
    allowed_status = [Event.OPEN, Event.WAITLIST, Event.CLOSED]

    def get_queryset(self):
        object_list = [event for event in super().get_queryset() if event.status in self.allowed_status]
        return object_list
