import datetime
from smtplib import SMTPRecipientsRefused

from captcha.fields import CaptchaField
from django import forms
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from django.views.generic import View
from django.views.generic.edit import CreateView, DeleteView

from registration.mixins import EventMixin
from registration.models import Event, WaitListEntry
from registration.utils.mails import (send_email_already_used_mail,
                                      send_email_confirmation_mail,
                                      send_newsletter_sign_up_mail)


class ViewSecurityMixin:
    @cached_property
    def view_being_spammed(self):
        # We assume that a lot of unconfirmed signups mean spam.
        return WaitListEntry.objects.filter(
            confirmed=False,
            created_at__gte=timezone.now() - datetime.timedelta(hours=24),
        ).count() > 20

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['view_being_spammed'] = self.view_being_spammed
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['view_being_spammed'] = self.view_being_spammed
        return context

    def form_valid(self, form):
        if (
            "http://" in self.waitlistentry.participant_name
            or "https://" in self.waitlistentry.participant_name
        ):
            messages.success(
                self.request,
                _(
                    "Almost done! We have just sent you an email "
                    "with the signup link."
                )
            )
            return redirect('event-list')


class FormSecurityMixin:
    def __init__(self, *args, **kwargs):
        view_being_spammed = kwargs.pop('view_being_spammed')
        super().__init__(*args, **kwargs)
        if view_being_spammed:
            self.fields['captcha'] = CaptchaField(label=_('Please enter the text in the image into the field:'))


class WaitListEntryCreate(EventMixin, ViewSecurityMixin, CreateView):
    class WaitListEntryCreateForm(FormSecurityMixin, forms.ModelForm):
        class Meta:
            model = WaitListEntry
            fields = ['participant_name', 'email_address', 'newsletter']

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['newsletter'].label = _('Yes, please sign me up!')

    model = WaitListEntry
    template_name = 'registration/waitlistentry-create.html'
    form_class = WaitListEntryCreateForm
    context_object_name = 'waitlistentry'
    allowed_status = [Event.OPEN, Event.WAITLIST, Event.CLOSED]

    def dispatch(self, request, *args, **kwargs):
        if self.event.status == Event.CLOSED:
            messages.warning(
                self.request,
                _("The registration for this event has already ended.")
            )
        elif self.event.status == Event.WAITLIST:
            messages.info(
                self.request,
                _("All seats for this event are taken, but you can sign up for the waitlist."),
            )
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if 'slug' in kwargs and not kwargs.get('slug') == self.event.slug:
            return redirect('waitlistentry-create', self.event.pk, self.event.slug)
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        # necessary to prevent signup for closed events
        if self.event.status == Event.OPEN or self.event.status == Event.WAITLIST:
            self.waitlistentry = form.save(commit=False)
            self.waitlistentry.event = self.event

            # Let ViewSecurityMixin.form_valid() handle the case of dodgy names
            response = super().form_valid(form)
            if response:
                return response

            # prevent leaking the email address by always displaying success message but
            # privately informing if email has already been used
            try:
                if self.waitlistentry.email_already_used:
                    send_email_already_used_mail(self.waitlistentry.email_already_used)
                else:
                    send_email_confirmation_mail(self.waitlistentry)
                    self.waitlistentry.save()
            except SMTPRecipientsRefused:
                form.add_error('email_address', _("Please enter a valid email address."))
                return super().form_invalid(form)

            messages.success(
                self.request,
                _(
                    "Almost done! We have just sent you an email "
                    "with the signup link."
                )
            )

        return redirect('event-list')


class WaitListEntryConfirm(EventMixin, View):
    template_name = 'registration/waitlistentry-confirm.html'
    allowed_status = [Event.OPEN, Event.WAITLIST]
    pk_url_kwarg = 'waitlist_pk'

    def get(self, request, *args, **kwargs):
        waitlistentry = get_object_or_404(WaitListEntry, uuid=self.kwargs.get('waitlist_pk'))
        waitlistentry.confirmed = True

        if waitlistentry.newsletter:
            send_newsletter_sign_up_mail(waitlistentry)
            # We don't need this information anymore after sending the email --> data minimization
            waitlistentry.newsletter = False

        if waitlistentry.event.status == Event.OPEN:
            # this has to happen after the event status check as the result is influenced by save
            waitlistentry.save()
            return redirect('registration-create', waitlistentry.uuid)
        else:
            waitlistentry.save()
            return render(request, self.template_name)


class WaitListSignOff(EventMixin, DeleteView):
    model = WaitListEntry
    template_name = 'registration/waitlist-sign-off.html'
    success_url = reverse_lazy('event-list')
    allowed_status = [Event.OPEN, Event.WAITLIST, Event.CLOSED]
    pk_url_kwarg = 'waitlist_pk'

    def post(self, request, *args, **kwargs):
        messages.success(
            self.request,
            _("You have successfully signed off.")
        )
        return super().post(self, request, *args, **kwargs)
