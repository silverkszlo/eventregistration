import datetime

from django.core.management.base import BaseCommand
from django.utils import timezone
from faker import Faker
from model_bakery import baker

from registration.models import Event, Registration, WaitListEntry

fake = Faker('de_DE')


def fake_event(max_participants, signup_deadline, signoff_deadline, start_date_time):
    event = baker.make(
        Event,
        title=fake.paragraph(nb_sentences=1, variable_nb_sentences=True),
        signup_deadline=timezone.now() + datetime.timedelta(days=signup_deadline),
        signoff_deadline=timezone.now() + datetime.timedelta(days=signoff_deadline),
        start_date_time=timezone.now() + datetime.timedelta(days=start_date_time),
        end_date_time=timezone.now() + datetime.timedelta(days=18),
        place=fake.city(),
        information=fake.paragraph(nb_sentences=4, variable_nb_sentences=True),
        questions=fake.paragraph(nb_sentences=1, variable_nb_sentences=True),
        max_participants=max_participants,
    )
    return event


def fake_event_paid(max_participants, signup_deadline, signoff_deadline, start_date_time):
    event = baker.make(
        Event,
        title=fake.paragraph(nb_sentences=1, variable_nb_sentences=True),
        signup_deadline=timezone.now() + datetime.timedelta(days=signup_deadline),
        signoff_deadline=timezone.now() + datetime.timedelta(days=signoff_deadline),
        start_date_time=timezone.now() + datetime.timedelta(days=start_date_time),
        end_date_time=timezone.now() + datetime.timedelta(days=18),
        place=fake.city(),
        information=fake.paragraph(nb_sentences=4, variable_nb_sentences=True),
        questions=fake.paragraph(nb_sentences=1, variable_nb_sentences=True),
        max_participants=max_participants,
        low_fee=10,
        medium_fee=20,
        high_fee=30,
    )
    return event


def fake_registration(event, signed_off, number):
    for i in range(number):
        baker.make(
            Registration,
            event=event,
            signed_off=signed_off,
            participant_name=fake.name(),
            email_address=fake.email(),
            paid_amount=0,
            comment=fake.paragraph(nb_sentences=3, variable_nb_sentences=True),
            answer=fake.paragraph(nb_sentences=1, variable_nb_sentences=True),
            participation_fee=event.medium_fee,
        )


def fake_waitlistentry(event, confirmed, number):
    for i in range(number):
        baker.make(
            WaitListEntry,
            event=event,
            confirmed=confirmed,
            participant_name=fake.name(),
            email_address=fake.email(),
            reminded_at=None,
        )


class Command(BaseCommand):
    help = 'Populate database with demo data.'

    def handle(self, *args, **options):
        if not Event.objects.exists():
            # create open events
            first_open = fake_event(
                max_participants=3, signup_deadline=10, signoff_deadline=7, start_date_time=17
            )
            fake_registration(event=first_open, signed_off=False, number=2)

            second_open = fake_event_paid(
                max_participants=5, signup_deadline=10, signoff_deadline=7, start_date_time=17
            )
            fake_registration(event=second_open, signed_off=False, number=1)

            fake_event(max_participants=15, signup_deadline=10, signoff_deadline=5, start_date_time=12)

            # create event in waitlist mode
            waiting = fake_event(
                max_participants=2, signup_deadline=10, signoff_deadline=7, start_date_time=17
            )
            fake_registration(event=waiting, signed_off=False, number=2)
            fake_waitlistentry(event=waiting, confirmed=True, number=2)

            # create closed event
            closed = fake_event(
                max_participants=2, signup_deadline=0, signoff_deadline=-1, start_date_time=7
            )
            fake_registration(event=closed, signed_off=False, number=2)

            # create event that is over
            over = fake_event(
                max_participants=2, signup_deadline=-6, signoff_deadline=-8, start_date_time=-2
            )
            fake_registration(event=over, signed_off=True, number=2)
