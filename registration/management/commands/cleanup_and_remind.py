from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from registration.models import Event, Registration, WaitListEntry
from registration.utils.mails import send_reminder_mail_about_expired_events


def cleanup():
    expiration_datetime = timezone.now() - settings.REGISTRATION_DELETION_PERIOD
    expired_registrations = Registration.objects.filter(event__end_date_time__lte=expiration_datetime).delete()

    waitlistentries = WaitListEntry.objects.filter(event__start_date_time__lte=timezone.now()).delete()
    events = Event.objects.filter(end_date_time__lt=timezone.now(), registrations=None).delete()

    return expired_registrations[0], waitlistentries[0], events[0]


def remind_about_future_cleanup():
    past_events = Event.objects.filter(end_date_time__lt=timezone.now())

    # filter out all events which are going to be deleted in e.g. 7 days
    medium_priority_events = past_events.filter(
        end_date_time__date=(
            timezone.now()
            - settings.REGISTRATION_DELETION_PERIOD
            + settings.REGISTRATION_DELETION_MEDIUM_REMAINING_PERIOD
        )
    )

    high_priority_events = past_events.filter(
        end_date_time__date=(
            timezone.now()
            - settings.REGISTRATION_DELETION_PERIOD
            + settings.REGISTRATION_DELETION_SMALL_REMAINING_PERIOD
        )
    )

    if medium_priority_events.exists() or high_priority_events.exists():
        send_reminder_mail_about_expired_events(medium_priority_events, high_priority_events)

    medium_priority_count = medium_priority_events.count()
    high_priority_count = high_priority_events.count()
    return medium_priority_count, high_priority_count


class Command(BaseCommand):
    help = 'Delete registrations after event expiration and remind about impending deletions.'

    def handle(self, *args, **options):
        expired_registrations, waitlistentries, events = cleanup()
        if any([expired_registrations, waitlistentries, events]):
            self.stdout.write(
                'Deleted {expired_registrations} expired registrations, {waitlistentries} '
                'waitlistentries and {events} events.'.format(
                    expired_registrations=expired_registrations,
                    waitlistentries=waitlistentries,
                    events=events,
                )
            )
        else:
            self.stdout.write('Nothing to delete.')

        medium_priority_count, high_priority_count = remind_about_future_cleanup()
        if medium_priority_count or high_priority_count:
            self.stdout.write(
                'Reminded about imminent deletion of {medium} and very imminent deletion of {high} events'.format(
                    medium=medium_priority_count, high=high_priority_count
                )
            )
        else:
            self.stdout.write('No impending event data deletions to remind about.')
