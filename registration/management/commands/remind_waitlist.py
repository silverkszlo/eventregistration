from django.core.management.base import BaseCommand
from django.utils import timezone

from registration.models import Event
from registration.utils.remind_waitlist import (
    get_next_waitlistentry_to_remind, remind_single_waitlistentry)


def remind_waitlist():
    counter = 0
    future_events = Event.objects.filter(start_date_time__gte=timezone.now())
    for event in future_events:
        for i in range(event.remaining_capacity):
            next_remindable = get_next_waitlistentry_to_remind(event)
            if next_remindable:
                counter += remind_single_waitlistentry(next_remindable)
    return counter


class Command(BaseCommand):
    help = 'Remind the next person on the waitlist via email.'

    def handle(self, *args, **options):
        counter = remind_waitlist()
        self.stdout.write("{} waitlistentry invitation emails sent.".format(counter))
