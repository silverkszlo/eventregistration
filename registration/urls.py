from django.urls import path

from registration.utils.feeds import RssEventFeed
from registration.views import (DataProtectionDeclaration, EventList,
                                GeneralTerms, LegalNotice, RegistrationCreate,
                                RegistrationSignOff, WaitListEntryConfirm,
                                WaitListEntryCreate, WaitListSignOff)

urlpatterns = [
    path('', EventList.as_view(), name='event-list'),
    path('general-terms', GeneralTerms.as_view(), name='general-terms'),
    path(
        'data-protection-declaration',
        DataProtectionDeclaration.as_view(),
        name='data-protection-declaration'
    ),

    path('legal-notice', LegalNotice.as_view(), name='legal-notice'),
    path('events/', EventList.as_view(), name='event-list'),

    path(
        '<int:pk>/',
        WaitListEntryCreate.as_view(),
        name='waitlistentry-create'
    ),
    path(
        '<int:pk>/<str:slug>/',
        WaitListEntryCreate.as_view(),
        name='waitlistentry-create'
    ),
    path(
        'confirm/<uuid:waitlist_pk>/',
        WaitListEntryConfirm.as_view(),
        name='waitlistentry-confirm'
    ),
    path(
        'waitlistentry/confirm/<uuid:waitlist_pk>/',
        WaitListEntryConfirm.as_view(),
        name='waitlistentry-confirm-legacy'
    ),
    path(
        'waitlist/sign-off/<uuid:waitlist_pk>/',
        WaitListSignOff.as_view(),
        name='waitlist-sign-off'
    ),

    path(
        'registration/<uuid:waitlist_pk>/',
        RegistrationCreate.as_view(),
        name='registration-create'
    ),
    path(
        'registration/sign-off/<uuid:registration_pk>/',
        RegistrationSignOff.as_view(),
        name='registration-sign-off'
    ),
    path('feed/', RssEventFeed()),
]
