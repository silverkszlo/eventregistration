from django.conf import settings
from django.core.mail import send_mail
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from registration import models


def remind_single_waitlistentry(waitlistentry: models.WaitListEntry) -> int:
    counter = 0
    signup_link = settings.EVR_HOST_URL + reverse('registration-create', args=[waitlistentry.uuid])
    delete_link = settings.EVR_HOST_URL + reverse('waitlist-sign-off', args=[waitlistentry.uuid])
    counter += send_mail(
        subject=_('A seat has been freed at the event'),
        message=settings.EVR_FREE_SPOT_ON_EVENT_MAIL_BODY.format(
            name=waitlistentry.participant_name,
            event=waitlistentry.event.title,
            date=waitlistentry.event.start_date_time.date(),
            signup_link=signup_link,
            deletelink=delete_link,
        ),
        from_email=None,
        recipient_list=[waitlistentry.email_address],
    )
    waitlistentry.reminded_at = timezone.now()
    waitlistentry.save()
    return counter


def get_next_waitlistentry_to_remind(event: models.Event) -> models.WaitListEntry | None:
    unreminded_waitlist_entries = models.WaitListEntry.objects.filter(
        event=event,
        confirmed=True,
        reminded_at__isnull=True,
    )
    if event.status == event.OPEN and unreminded_waitlist_entries.exists():
        return unreminded_waitlist_entries.earliest('created_at')
