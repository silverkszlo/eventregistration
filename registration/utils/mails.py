from typing import TYPE_CHECKING, Union

from django.conf import settings
from django.core.mail import EmailMessage
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

if TYPE_CHECKING:
    from django.db.models.query import QuerySet

    from registration.models import Event, Registration, WaitListEntry


def send_email_confirmation_mail(waitlistentry: 'WaitListEntry') -> int:
    subject = _('Please confirm your email address')
    from_email = None
    to_email = waitlistentry.email_address

    confirmation_link = settings.EVR_HOST_URL + reverse('waitlistentry-confirm', args=[waitlistentry.uuid])
    delete_link = settings.EVR_HOST_URL + reverse('waitlist-sign-off', args=[waitlistentry.uuid])
    email_body = settings.EVR_EMAIL_CONFIRMATION_MAIL_BODY.format(
        name=waitlistentry.participant_name,
        event=waitlistentry.event.title,
        date=waitlistentry.event.start_date_time.date(),
        confirmlink=confirmation_link,
        deletelink=delete_link,
    )
    email = EmailMessage(
        subject, email_body, from_email, to=[to_email]
    )
    return email.send()


def send_email_already_used_mail(waitlistentry_or_registration: Union['Registration', 'WaitListEntry']) -> int:
    subject = _(
        'Your email address has already been used to sign up for the event {event} on {date}'
    ).format(
        event=waitlistentry_or_registration.event.title,
        date=waitlistentry_or_registration.event.start_date_time.date())
    from_email = None
    to_email = waitlistentry_or_registration.email_address
    email_body = settings.EVR_EMAIL_ALREADY_USED_MAIL_BODY.format(
        name=waitlistentry_or_registration.participant_name,
        event=waitlistentry_or_registration.event.title,
        date=waitlistentry_or_registration.event.start_date_time.date(),
    )
    email = EmailMessage(subject, email_body, from_email, to=[to_email])
    return email.send()


def send_reminder_mail_about_expired_events(
    medium_priority_events: 'QuerySet[Event]', high_priority_events: 'QuerySet[Event]'
) -> int:
    subject = settings.REGISTRATION_DELETION_REMINDER_MAIL_SUBJECT
    from_email = None
    to_email = settings.ORGANIZER_CONTACT_EMAIL

    email_body = settings.REGISTRATION_DELETION_REMINDER_MAIL_BODY.format(
        deletion_period_small=settings.REGISTRATION_DELETION_SMALL_REMAINING_PERIOD.days,
        high_priority_events="\n".join(str(event) for event in high_priority_events),
        deletion_period_medium=settings.REGISTRATION_DELETION_MEDIUM_REMAINING_PERIOD.days,
        medium_priority_events="\n".join(str(event) for event in medium_priority_events)
    )

    email = EmailMessage(
        subject, email_body, from_email, to=[to_email], bcc=[settings.ADMIN_EMAIL]
    )
    return email.send()


def send_organizer_sign_off_mail(event: 'Event') -> int:
    subject = settings.ORGANIZER_SIGN_OFF_MAIL_SUBJECT.format(
        event=event.title, date=event.start_date_time.date()
    )
    from_email = None
    to_email = settings.ORGANIZER_CONTACT_EMAIL
    email_body = settings.ORGANIZER_SIGN_OFF_MAIL_BODY.format(
        event=event.title,
        date=event.start_date_time.date(),
        remaining_capacity_only_registrations=event.remaining_capacity_only_registrations,
        waiting=event.waiting,
    )

    email = EmailMessage(subject, email_body, from_email, to=[to_email])
    return email.send()


def send_final_confirmation_mail(registration: 'Registration') -> int:
    subject = _('Your registration has been confirmed successfully')
    from_email = None
    to_email = registration.email_address
    delete_link = settings.EVR_HOST_URL + reverse('registration-sign-off', args=[registration.uuid])

    if registration.event.low_fee:
        body = settings.EVR_FINAL_CONFIRMATION_MAIL_BODY_PAID_EVENT
    else:
        body = settings.EVR_FINAL_CONFIRMATION_MAIL_BODY_FREE_EVENT

    email_body = body.format(
        name=registration.participant_name,
        event=registration.event.title,
        date=registration.event.start_date_time.date(),
        order_id=registration.order_id,
        delete_link=delete_link,
        signoff_deadline=(
            str(registration.event.signoff_deadline.date())
            + ', '
            + str(registration.event.signoff_deadline.time().strftime('%H:%M'))
        )
    )
    email = EmailMessage(subject, email_body, from_email, to=[to_email])
    return email.send()


def send_newsletter_sign_up_mail(waitlistentry: 'WaitListEntry') -> int:
    subject = settings.NEWSLETTER_SIGN_UP_SUBJECT
    from_email = None
    to_email = settings.ORGANIZER_CONTACT_EMAIL

    email_body = settings.NEWSLETTER_SIGN_UP_BODY.format(
        email_address=waitlistentry.email_address
    )

    email = EmailMessage(subject, email_body, from_email, to=[to_email])
    return email.send()
