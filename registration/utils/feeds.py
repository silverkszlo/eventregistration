from django.contrib.syndication.views import Feed
from django.utils import timezone

from registration.models import Event


class RssEventFeed(Feed):
    title = "New Educat events"
    link = "/feed/"
    description = "New events by the Educat collective."

    def items(self):
        return Event.objects.filter(signup_deadline__gte=timezone.now()).order_by('-start_date_time')

    def item_title(self, item):
        return item.title
