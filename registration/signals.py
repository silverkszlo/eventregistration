def create_order_id(sender, instance, **kwargs):
    from registration.models import Registration

    if sender is Registration and not instance.order_id:
        instance.order_id = instance.create_unique_order_id()
        instance.save()


def remind_waitlist_base(event, **kwargs):
    from django.conf import settings
    from django.utils import timezone

    from .models import WaitListEntry
    from .utils.remind_waitlist import (get_next_waitlistentry_to_remind,
                                        remind_single_waitlistentry)

    next_remindable = get_next_waitlistentry_to_remind(event)
    if next_remindable:
        remind_single_waitlistentry(next_remindable)
        already_reminded_unrecently = WaitListEntry.objects.filter(
            event=event,
            reminded_at__isnull=False,
            reminded_at__lte=timezone.now() - settings.WAITLIST_REMINDER_PERIOD,
        )
        for waitlistentry in already_reminded_unrecently:
            remind_single_waitlistentry(waitlistentry)


def remind_waitlist_on_registration_delete(sender, instance, **kwargs):
    from .models import Registration, WaitListEntry

    if sender is WaitListEntry or sender is Registration:
        remind_waitlist_base(instance.event, **kwargs)


def remind_waitlist_on_registration_save(sender, instance, **kwargs):
    from .models import Registration

    if sender is Registration and (instance.signed_off or instance.guestlist):
        remind_waitlist_base(instance.event, **kwargs)


def remind_waitlist_on_event_save(sender, instance, **kwargs):
    from .models import Event

    if sender is Event and instance.status != Event.OVER:
        remind_waitlist_base(instance, **kwargs)
