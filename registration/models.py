import uuid

from django.conf import settings
from django.core.exceptions import PermissionDenied, ValidationError
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from .model_fields import LowercaseEmailField


class Event(models.Model):
    OVER = ('Over', _('Over'))
    CLOSED = ('Closed', _('Closed'))
    OPEN = ('Open', _('Open'))
    WAITLIST = ('Waitlist', _('Waitlist'))

    # The max_length the slug needs to be at least 11 chars bigger than the max_length
    # of title (see save() method)
    slug = models.SlugField(_('Slug'), max_length=110, blank=True)
    title = models.CharField(_('Event title'), max_length=90)
    signup_deadline = models.DateTimeField(
        _('Signup deadline'),
        help_text=_(
            'After the signup deadline registrations are no longer possible. '
            'Also people from the waitlist can not join the event anymore.'
        )
    )
    signoff_deadline = models.DateTimeField(
        _('Signoff deadline'),
        help_text=_(
            'For paid events, participants will be informed upon registration that they '
            'may have to pay part of the fee if they cancel after the deadline.'
        )
    )
    max_participants = models.PositiveSmallIntegerField(_('Max. participants'))
    start_date_time = models.DateTimeField(_('Start time'))
    end_date_time = models.DateTimeField(_('End time'))
    place = models.CharField(_('Place'), max_length=200, blank=True)
    information = models.TextField(_('Event information'), max_length=2000, blank=True)
    questions = models.TextField(_('Questions'), max_length=1000, blank=True)
    low_fee = models.PositiveSmallIntegerField(_('Low fee'), blank=True, null=True)
    medium_fee = models.PositiveSmallIntegerField(_('Medium fee'), blank=True, null=True)
    high_fee = models.PositiveSmallIntegerField(_('High fee'), blank=True, null=True)

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')
        ordering = ['start_date_time']

    def __str__(self):
        return f"{self.start_date_time.date()} {self.title}"

    def get_absolute_url(self):
        return reverse('waitlistentry-create', args=[str(self.pk)])

    def clean(self):
        if self.start_date_time and self.end_date_time:
            if self.start_date_time >= self.end_date_time:
                raise ValidationError(_('Please choose an end date and time which are after start date and time!'))
        if self.signup_deadline and self.signoff_deadline:
            if self.signup_deadline < self.signoff_deadline:
                raise ValidationError(_('The signoff deadline should be before the signup deadline!'))
        fee_fields = [self.low_fee, self.medium_fee, self.high_fee]
        if any(fee_fields) and not all(fee_fields):
            raise ValidationError(_('Please provide values for all pricing categories!'))

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(str(self.start_date_time.date()) + '-' + self.title)
        return super().save(*args, **kwargs)

    @property
    def status(self, *args, **kwargs):
        if timezone.now() >= self.start_date_time:
            return self.OVER
        elif self.signup_deadline < timezone.now():
            return self.CLOSED
        elif self.remaining_capacity > 0:
            return self.OPEN
        else:
            return self.WAITLIST

    @property
    def remaining_capacity(self):
        """
        Calculate remaining capacity by counting actual or potential registrations
        """
        within_reminder_period = self.waitlist_entries.filter(
            confirmed=True,
            reminded_at__gte=timezone.now() - settings.WAITLIST_REMINDER_PERIOD,
        ).count()
        return self.remaining_capacity_only_registrations - within_reminder_period

    @property
    def remaining_capacity_only_registrations(self):
        """
        Calculate remaining capacity by counting actual registrations without
        considering recently reminded waitlist entries
        """
        return self.max_participants - self.registered

    # This is a workaround as we can not give a verbose_name to a property
    def _capacity_verbose(self):
        return str(self.remaining_capacity)

    _capacity_verbose.short_description = _('Remaining capacity')
    _capacity_verbose.admin_order_field = 'capacity__verbose'
    capacity_verbose = property(_capacity_verbose)

    @property
    def registered(self):
        return (
            self.registrations.filter(signed_off=False, guestlist=False).count()
        )

    def _registered_verbose(self):
        return str(self.registered)

    _registered_verbose.short_description = _('Registered')
    _registered_verbose.admin_order_field = 'registered__verbose'
    registered_verbose = property(_registered_verbose)

    @property
    def waiting(self):
        return self.waitlist_entries.filter(confirmed=True).count()

    def _waiting_verbose(self):
        return str(self.waiting)

    _waiting_verbose.short_description = _('Waiting')
    _waiting_verbose.admin_order_field = 'waiting__verbose'
    waiting_verbose = property(_waiting_verbose)

    @property
    def guestlist(self):
        return (
            self.registrations.filter(signed_off=False, guestlist=True).count()
        )

    def _guestlist_verbose(self):
        return str(self.guestlist)

    _guestlist_verbose.short_description = _('Guestlist')
    _guestlist_verbose.admin_order_field = 'guestlist__verbose'
    guestlist_verbose = property(_guestlist_verbose)

    @property
    def free_cancellation_period(self):
        return self.signoff_deadline > timezone.now()


class AbstractRegistration(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    participant_name = models.CharField(_('Name'), max_length=200)
    email_address = LowercaseEmailField(_('Email address'))
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)

    class Meta:
        abstract = True

    def __str__(self):
        return f"{self.participant_name} {self.email_address}"

    @cached_property
    def email_already_used(self):
        return (
            Registration.objects.filter(
                event=self.event, email_address=self.email_address
            )
            or WaitListEntry.objects.filter(
                event=self.event,
                email_address=self.email_address,
                confirmed=True,
            ).exclude(uuid=self.uuid)
        ).first()


class WaitListEntry(AbstractRegistration):
    event = models.ForeignKey(
        Event, verbose_name=_('Event'), on_delete=models.CASCADE, related_name='waitlist_entries'
    )
    reminded_at = models.DateTimeField(_('Reminded at'), null=True, blank=True)
    confirmed = models.BooleanField(_('Confirmed'), default=False)
    newsletter = models.BooleanField(_('Newsletter subscription'), default=False)

    class Meta:
        verbose_name = _('Waitlistentry')
        verbose_name_plural = _('Waitlistentries')
        ordering = ['created_at']


class Registration(AbstractRegistration):
    order_id = models.CharField(_('Order ID'), max_length=20, unique=True, editable=False)
    event = models.ForeignKey(Event, verbose_name=_('Event'), on_delete=models.CASCADE, related_name='registrations')
    paid_amount = models.FloatField(_('Paid amount'), default=0)
    comment = models.TextField(_('Comment'), max_length=1000, blank=True)
    answer = models.TextField(_('Answer'), max_length=1000, blank=True)
    signed_off = models.BooleanField(_('Signed off after deadline'), default=False)
    guestlist = models.BooleanField(
        _('Guestlist'),
        default=False,
        help_text=_(
            "People on the guestlist do not count as regular registrations. Marking an existing "
            "registration as a guestlist will accordingly free a seat. People on the guestlist "
            "will not receive any automated emails. You are responsible for all communication, "
            "including handling deletion requests."
        )
    )
    participation_fee = models.PositiveSmallIntegerField(_('Participation fee'), blank=True, null=True)

    class Meta:
        verbose_name = _('Registration')
        verbose_name_plural = _('Registrations')
        ordering = ['participant_name']

    def save(self, *args, **kwargs):
        if (
            not Registration.objects.filter(uuid=self.uuid).exists()
            and self.event.remaining_capacity_only_registrations <= 0
            and not self.guestlist
        ):
            raise PermissionDenied()
        else:
            super(Registration, self).save(*args, **kwargs)

    def clean(self, *args, **kwargs):
        if self.paid_amount < 0:
            raise ValidationError(_('Please enter a valid value for paid amount!'))

    def create_unique_order_id(self):
        from registration.utils import create_random_string
        order_id = "-".join(
            [str(self.event.start_date_time.date()),
             create_random_string()]
        )
        if Registration.objects.all().filter(order_id=order_id).exists():
            order_id = self.create_unique_order_id()
        return order_id
