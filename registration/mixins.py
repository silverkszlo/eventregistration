from django.shortcuts import get_object_or_404, redirect

from .models import Event, Registration, WaitListEntry


class EventMixin():
    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        if 'pk' in self.kwargs:
            self.event = get_object_or_404(Event, pk=self.kwargs.get('pk'))
        elif 'registration_pk' in self.kwargs:
            registration = get_object_or_404(Registration, uuid=self.kwargs['registration_pk'])
            self.event = registration.event
        elif 'waitlist_pk' in self.kwargs:
            waitlistentry = get_object_or_404(WaitListEntry, uuid=self.kwargs.get('waitlist_pk'))
            self.event = waitlistentry.event

    def dispatch(self, request, *args, **kwargs):
        if self.event.status not in self.allowed_status:
            if self.event.status == Event.OVER:
                return redirect('event-list')
            else:
                return redirect('waitlistentry-create', self.event.uuid)
        else:
            return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['event'] = self.event
        return context
