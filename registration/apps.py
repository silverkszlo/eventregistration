from django.apps import AppConfig
from django.db.models.signals import post_delete, post_save, pre_save

from . import signals


class RegistrationConfig(AppConfig):
    name = 'registration'

    def ready(self):
        pre_save.connect(signals.create_order_id)
        post_save.connect(signals.remind_waitlist_on_registration_save)
        post_save.connect(signals.remind_waitlist_on_event_save)
        post_delete.connect(signals.remind_waitlist_on_registration_delete)
