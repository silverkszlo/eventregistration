# Generated by Django 3.2.12 on 2022-02-18 18:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0022_remove_registration_payment_received'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='registration_deadline',
            field=models.DateTimeField(help_text='After the registration deadline registrations are no longer possible. People on the waiting list will also not be notified thereafter. For paid events, participants will be informed upon registration that they may have to pay part of the fee if they cancel after the deadline.', verbose_name='Registration deadline'),
        ),
    ]
