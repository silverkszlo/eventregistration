# Generated by Django 3.2.6 on 2021-08-17 13:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0019_alter_event_information'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registration',
            name='event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='registrations', to='registration.event', verbose_name='Event'),
        ),
        migrations.AlterField(
            model_name='waitlistentry',
            name='event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='waitlist_entries', to='registration.event', verbose_name='Event'),
        ),
    ]
