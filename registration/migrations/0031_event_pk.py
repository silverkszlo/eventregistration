from django.db import migrations, models
import django.db.models.deletion

import uuid


def copy_event(apps, schema_editor):
    EventNew = apps.get_model("registration", "EventNew")
    Event = apps.get_model("registration", "Event")

    id = 1
    for event in Event.objects.all():
        new_event = EventNew.objects.create(
            id=id,
            uuid=event.uuid,
            title=event.title,
            type=event.type,
            signup_deadline=event.signup_deadline,
            signoff_deadline=event.signoff_deadline,
            max_participants=event.max_participants,
            start_date_time=event.start_date_time,
            end_date_time=event.end_date_time,
            place=event.place,
            information=event.information,
            questions=event.questions,
        )
        assert event.uuid == new_event.uuid
        id += 1


def set_new_foreign_keys(apps, schema_editor):
    EventNew = apps.get_model("registration", "EventNew")

    Registration = apps.get_model("registration", "Registration")
    for registration in Registration.objects.all():
        registration.event_new = EventNew.objects.get(uuid=registration.event.uuid)
        registration.save()
        assert registration.event.uuid == registration.event_new.uuid

    WaitListEntry = apps.get_model("registration", "WaitListEntry")
    for waitlistentry in WaitListEntry.objects.all():
        waitlistentry.event_new = EventNew.objects.get(uuid=waitlistentry.event.uuid)
        waitlistentry.save()
        assert waitlistentry.event.uuid == waitlistentry.event_new.uuid


def dummy_reverse(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0030_waitlistentry_newsletter'),
    ]

    operations = [
        # Create EventNew model
        migrations.CreateModel(
            name='EventNew',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('title', models.CharField(max_length=200, verbose_name='Event title')),
                ('type', models.CharField(choices=[('W', 'Workshop'), ('H', 'Hike'), ('WE', 'Online-Seminar'), ('TA', 'Trip abroad'), ('CT', 'City Tour')], max_length=100, verbose_name='Type')),
                ('signup_deadline', models.DateTimeField(help_text='After the signup deadline registrations are no longer possible. Also people from the waitlist can not join the event anymore.', verbose_name='Signup deadline')),
                ('signoff_deadline', models.DateTimeField(help_text='For paid events, participants will be informed upon registration that they may have to pay part of the fee if they cancel after the deadline.', verbose_name='Signoff deadline')),
                ('max_participants', models.PositiveSmallIntegerField(verbose_name='Max. participants')),
                ('start_date_time', models.DateTimeField(verbose_name='Start time')),
                ('end_date_time', models.DateTimeField(verbose_name='End time')),
                ('place', models.CharField(blank=True, max_length=200, verbose_name='Place')),
                ('information', models.TextField(blank=True, max_length=2000, verbose_name='Event information')),
                ('questions', models.TextField(blank=True, max_length=1000, verbose_name='Questions')),
            ],
            options={'ordering': ['start_date_time'], 'verbose_name': 'Event', 'verbose_name_plural': 'Events'},
        ),
        # and copy data
        migrations.RunPython(copy_event, dummy_reverse),

        # Create foreign keys to EventNew model
        migrations.AddField(
            model_name='registration',
            name='event_new',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='registrations',
                to='registration.eventnew',
                verbose_name='EventNew',
                null=True
            ),
        ),
        migrations.AddField(
            model_name='waitlistentry',
            name='event_new',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='waitlist_entries',
                to='registration.eventnew',
                verbose_name='EventNew',
                null=True
            ),
        ),
        migrations.RunPython(set_new_foreign_keys, dummy_reverse),

        # Delete Event model, including foreign keys pointing to it
        migrations.RemoveField(
            model_name='registration',
            name='event',
        ),
        migrations.RemoveField(
            model_name='waitlistentry',
            name='event',
        ),
        migrations.DeleteModel('Event'),

        # Rename EventNew to Event, including foreign keys pointing to it
        migrations.RenameModel('EventNew', 'Event'),

        migrations.RenameField('Registration', 'event_new', 'event'),
        migrations.AlterField(
            model_name='registration',
            name='event',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='registrations',
                to='registration.event',
                verbose_name='Event',
                null=True
            ),
        ),

        migrations.RenameField('WaitListEntry', 'event_new', 'event'),
        migrations.AlterField(
            model_name='waitlistentry',
            name='event',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='waitlist_entries',
                to='registration.event',
                verbose_name='Event',
                null=True
            ),
        ),

        # Remove the now obsolete UUID-field
        migrations.RemoveField(
            model_name='event',
            name='uuid'
        ),
    ]


