# Generated by Django 3.2.1 on 2021-08-06 09:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0017_merge_20210729_1519'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='registration',
            options={'ordering': ['-payment_received', 'participant_name'], 'verbose_name': 'Registration', 'verbose_name_plural': 'Registrations'},
        ),
        migrations.AlterModelOptions(
            name='waitlistentry',
            options={'ordering': ['created_at'], 'verbose_name': 'Waitlistentry', 'verbose_name_plural': 'Waitlistentries'},
        ),
    ]
