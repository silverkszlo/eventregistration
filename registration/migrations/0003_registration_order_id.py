# Generated by Django 3.1 on 2020-10-10 15:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0002_auto_20201009_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='registration',
            name='order_id',
            field=models.CharField(default='aaa', editable=False, max_length=20, unique=True, verbose_name='Order ID'),
            preserve_default=False,
        ),
    ]
