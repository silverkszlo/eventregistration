# Generated by Django 3.1.3 on 2021-04-04 11:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0009_payment_received_read_only'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='information',
            field=models.TextField(blank=True, max_length=1000, verbose_name='Event information'),
        ),
    ]
