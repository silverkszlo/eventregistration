from django.contrib import admin
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _
from import_export import fields, resources
from import_export.admin import ExportMixin
from import_export.fields import Field
from import_export.formats import base_formats
from import_export.widgets import BooleanWidget, ForeignKeyWidget

from .models import Event, Registration, WaitListEntry


class PaymentReceivedFilter(admin.SimpleListFilter):
    title = _('Payment received')
    parameter_name = 'payment_received'

    def lookups(self, request, model_admin):
        return (
            ('Yes', _('Yes')),
            ('No', _('No')),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            return queryset.filter(paid_amount__gt=0)
        elif value == 'No':
            return queryset.filter(paid_amount=0)
        return queryset


class WaitListEntryInline(admin.TabularInline):
    model = WaitListEntry
    view_on_site = False
    show_change_link = True
    fields = ('participant_name', 'email_address')
    readonly_fields = ('participant_name', 'email_address')
    # number of blank forms displayed below the list of registrations
    extra = 0

    def get_queryset(self, request):
        queryset = super().get_queryset(request).filter(confirmed=True)
        return queryset

    def has_add_permission(self, request, obj=None):
        return False


class RegistrationInline(admin.TabularInline):
    model = Registration
    view_on_site = False
    show_change_link = True
    fields = (
        'participant_name',
        'email_address',
        'created_at',
        'order_id',
        'paid_amount',
        'guestlist',
        'comment',
        'answer',
        'signed_off',
        'participation_fee',
    )
    readonly_fields = (
        'order_id',
        'participant_name',
        'email_address',
        'created_at',
        'comment',
        'answer',
        'signed_off',
        'participation_fee',
    )

    # number of blank forms displayed below the list of waitlist_entries
    extra = 0

    def has_add_permission(self, request, obj=None):
        return False


class PaymentBoolWidget(BooleanWidget):
    def render(self, value, obj=None):
        """
        Render 'True' and 'False' as 'paid' and 'pending' in the exported spreadsheet.
        """
        return _('Paid') if value else _('Pending')


class StartDateForeignKeyWidget(ForeignKeyWidget):
    def render(self, value, obj=None):
        """
        Render only the date value of an event's start_date_time-attribute
        to the exported spreadsheet.
        """
        return value.start_date_time.date()


class RegistrationResource(resources.ModelResource):
    """
    Define fields, column names and export order for the exported spreadsheet.
    """
    # remove this in case the Registration model
    # gets its own blank field for fee
    # fee = Field(column_name=_('Amount'))

    # rename columns for spreadsheet export
    participant_name = Field(
        attribute='participant_name',
        column_name=_('Name'),
    )
    email_address = Field(
        attribute='email_address',
        column_name=_('Email address'),
    )
    paid_amount = Field(
        attribute='paid_amount',
        column_name=_(_('Paid amount')),
    )
    comment = Field(
        attribute='comment',
        column_name=_('Comment'),
    )
    guestlist = Field(
        attribute='guestlist',
        column_name=_('Guestlist'),
    )
    answer = fields.Field(
        attribute='answer',
        column_name=_('Answer'),
    )
    event_title = fields.Field(
        column_name=_('Event'),
        attribute='event',
        widget=ForeignKeyWidget(Event, 'title'),
    )
    event_start_date = fields.Field(
        column_name=_('Date (Start)'),
        attribute='event',
        widget=StartDateForeignKeyWidget(Event, 'start_date_time'),
    )
    signed_off_after_deadline = fields.Field(
        attribute='signed_off',
        column_name=_('Signed off after deadline'),
    )

    class Meta:
        model = Registration
        fields = (
            'event_title',
            'event_start_date',
            'participant_name',
            'email_address',
            'paid_amount',
            'guestlist',
            'comment',
            'answer',
        )
        export_order = (
            'event_title',
            'event_start_date',
            'participant_name',
            'email_address',
            'paid_amount',
            'guestlist',
            'comment',
            'answer',
        )


# register import-export in the admin interface
class RegistrationAdmin(ExportMixin, admin.ModelAdmin):
    view_on_site = False
    readonly_fields = (
        'order_id',
        'created_at',
    )
    list_display = (
        'participant_name',
        'email_address',
        'created_at',
        'order_id',
        'paid_amount',
        'guestlist',
        'signed_off',
        'participation_fee'
    )
    list_filter = [
        PaymentReceivedFilter,
        'guestlist',
        'created_at',
        'signed_off',
        'event',
        'participation_fee',
    ]

    actions = ['send_email_to_selected']

    fieldsets = (
        (_('Contact details'), {
            'fields': (
                'order_id',
                'participant_name',
                'email_address',
            )
        }),
        (_('Booking information'), {
            'fields': (
                'paid_amount',
                'participation_fee',
                'guestlist',
            )
        }),
        (_('Details'), {
            'fields': (
                'event',
                'created_at',
                'comment',
                'answer',
            ),
        }),
    )
    search_fields = ['participant_name__startswith', 'email_address__startswith', 'order_id__startswith']
    resource_class = RegistrationResource

    def get_export_formats(self):
        formats = (
            base_formats.CSV,
            base_formats.XLSX,
            base_formats.ODS,
        )
        return [f for f in formats if f().can_export()]

    @admin.action(description='Send email to selected participants')
    def send_email_to_selected(self, request, queryset):
        selected_recipients = [obj.email_address for obj in queryset]
        mail_to_link = "mailto:?&bcc={}".format(','.join(obj for obj in selected_recipients))
        return render(
            request,
            'admin/send-email.html',
            context={'selected_recipients': selected_recipients, 'mail_to_link': mail_to_link}
        )


class WaitListEntryAdmin(admin.ModelAdmin):
    view_on_site = False
    readonly_fields = ('created_at', 'reminded_at', 'event',)
    list_display = ('participant_name', 'email_address', 'created_at', 'reminded_at', 'confirmed')
    list_filter = ['confirmed', 'created_at', 'reminded_at', 'event']
    search_fields = ['participant_name__startswith', 'email_address__startswith']
    fieldsets = (
        (_('Details'), {
            'fields': (
                'participant_name',
                'email_address',
                'event',
                'created_at',
                'reminded_at',
            )
        }),
    )

    def has_add_permission(self, request):
        return False


class EventAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'max_participants',
        'registered_verbose',
        'guestlist_verbose',
        'waiting_verbose',
        'capacity_verbose',
        'start_date_time',
        'end_date_time',
        'signup_deadline',
        'signoff_deadline',
        'place',
    )
    fields = (
        'title',
        'place',
        'max_participants',
        'start_date_time',
        'end_date_time',
        'signup_deadline',
        'signoff_deadline',
        'information',
        'questions',
        'low_fee',
        'medium_fee',
        'high_fee',

    )
    search_fields = ['title__startswith', 'place__startswith']
    ordering = ['start_date_time']

    # disable sorting by model properties registered_verbose,
    # waiting_verbose, and capacity_verbose to avoid field error
    sortable_by = (
        'title',
        'max_participants',
        'start_date_time',
        'end_date_time',
        'place',
    )
    inlines = [RegistrationInline, WaitListEntryInline]


admin.site.register(Event, EventAdmin)
admin.site.register(WaitListEntry, WaitListEntryAdmin)
admin.site.register(Registration, RegistrationAdmin)
