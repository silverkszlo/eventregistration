import datetime

import pytest
from django.db.models import EmailField
from django.utils import timezone
from model_bakery import baker

from registration.model_fields import LowercaseEmailField
from registration.models import Event, Registration, WaitListEntry

baker.generators.add(LowercaseEmailField, baker.generators.default_mapping[EmailField])


@pytest.fixture
def future_event_one_seat(db):
    start = timezone.now() + datetime.timedelta(days=10)
    later = timezone.now() + datetime.timedelta(days=8)
    before = timezone.now() + datetime.timedelta(days=3)
    event = baker.make(
        Event,
        start_date_time=start,
        max_participants=1,
        signup_deadline=later,
        signoff_deadline=before,
    )
    return event


@pytest.fixture
def registration(future_event_one_seat):
    return baker.make(Registration, event=future_event_one_seat)


@pytest.fixture
def unconfirmed_waitlistentry(future_event_one_seat):
    return baker.make(WaitListEntry, event=future_event_one_seat)


@pytest.fixture
def confirmed_waitlistentry(future_event_one_seat):
    return baker.make(WaitListEntry, event=future_event_one_seat, confirmed=True)


@pytest.fixture
def confirmed_reminded_waitlistentry(future_event_one_seat):
    return baker.make(WaitListEntry, event=future_event_one_seat, confirmed=True, reminded_at=timezone.now())
