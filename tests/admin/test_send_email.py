
from django.shortcuts import reverse
from model_bakery import baker

from registration.models import Registration


def test_send_custom_email(admin_client, registration):

    data = {
        'action': 'send_email_to_selected',
        '_selected_action': registration.uuid
    }
    change_url = reverse('admin:registration_registration_changelist')
    response = admin_client.post(change_url, data)
    assert registration.email_address.lower() in response.context['selected_recipients']
    assert registration.email_address.lower() in response.context['mail_to_link']
    assert response.status_code == 200


def test_send_email_only_to_selected_participants(admin_client):

    not_selected = baker.make(Registration, _quantity=4)
    selected = baker.make(Registration, _quantity=3)

    data = {
        'action': 'send_email_to_selected',
        '_selected_action': [registration.uuid for registration in selected]
    }
    change_url = reverse('admin:registration_registration_changelist')
    response = admin_client.post(change_url, data)
    response_selected = response.context['selected_recipients']
    response_mailto_link = response.context['mail_to_link']

    for registration in selected:
        assert registration.email_address.lower() in response_selected \
               and registration.email_address.lower() in response_mailto_link
    for registration in not_selected:
        assert registration.email_address.lower() not in response_selected \
               and registration.email_address not in response_mailto_link
    assert response.status_code == 200
