import datetime

from django.core import mail
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time
from model_bakery import baker

from registration.management.commands.remind_waitlist import Command
from registration.models import WaitListEntry

cmd = Command()


def test_remind_waitlist_command_reminds_waitlist(future_event_one_seat, confirmed_waitlistentry):
    now = timezone.now()
    with freeze_time(now):
        cmd.handle()
        confirmed_waitlistentry.refresh_from_db()
    assert confirmed_waitlistentry.reminded_at == now
    assert len(mail.outbox) == 1


def test_remind_waitlist_command_reminds_next_waitlist_entry(future_event_one_seat, confirmed_waitlistentry):
    baker.make(
        WaitListEntry,
        event=future_event_one_seat,
        confirmed=True,
        reminded_at=timezone.now()
    )

    later = timezone.now() + datetime.timedelta(days=2)
    with freeze_time(later):
        cmd.handle()
        confirmed_waitlistentry.refresh_from_db()
    assert confirmed_waitlistentry.reminded_at == later
    assert len(mail.outbox) == 1


def test_remind_waitlist_command_doesnt_remind_unconfirmed_waitlist_entries(
    future_event_one_seat, unconfirmed_waitlistentry
):
    cmd.handle()
    unconfirmed_waitlistentry.refresh_from_db()
    assert unconfirmed_waitlistentry.reminded_at is None


def test_remind_waitlist_command_doesnt_remind_too_many_waitlist_entries(
    future_event_one_seat, confirmed_waitlistentry
):
    confirmed_waitlistentry_2 = baker.make(WaitListEntry, event=future_event_one_seat, confirmed=True)

    now = timezone.now()
    with freeze_time(now):
        cmd.handle()
        confirmed_waitlistentry.refresh_from_db()
        confirmed_waitlistentry_2.refresh_from_db()
    assert confirmed_waitlistentry.reminded_at == now
    assert confirmed_waitlistentry_2.reminded_at is None


def test_remind_waitlist_command_doesnt_reremind_recently_reminded_waitlist_entries(
    future_event_one_seat, confirmed_waitlistentry
):
    now = timezone.now()
    with freeze_time(now):
        cmd.handle()

    later = timezone.now()
    with freeze_time(later):
        confirmed_waitlistentry.refresh_from_db()
        cmd.handle()
        confirmed_waitlistentry.refresh_from_db()
        assert confirmed_waitlistentry.reminded_at == now


def test_signup_link_is_correct_and_working(client, future_event_one_seat, confirmed_waitlistentry):
    now = timezone.now()
    with freeze_time(now):
        cmd.handle()

    url = reverse('registration-create', args=[confirmed_waitlistentry.uuid])
    response = client.get(url)

    assert response.status_code >= 200
    assert url in mail.outbox[0].body
