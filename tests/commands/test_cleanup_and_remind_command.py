import datetime

import pytest
from django.core import mail
from django.utils import timezone
from freezegun import freeze_time
from model_bakery import baker

from registration.management.commands.cleanup_and_remind import Command
from registration.models import Event, Registration, WaitListEntry

cmd = Command()


@pytest.mark.django_db
def test_registration_deleted_after_expiration():
    event = baker.make(Event, max_participants=1, end_date_time=timezone.now())
    baker.make(Registration, event=event)
    registration_deletion_period = datetime.timedelta(days=30)

    day_after_expiration = event.end_date_time + registration_deletion_period + datetime.timedelta(days=1)
    with freeze_time(day_after_expiration):
        cmd.handle()
        assert not Registration.objects.exists()


@pytest.mark.django_db
def test_waitlist_deleted_after_event_start():
    event = baker.make(Event, max_participants=1, start_date_time=timezone.now())
    baker.make(WaitListEntry, event=event)
    cmd.handle()
    assert not WaitListEntry.objects.exists()


@pytest.mark.django_db
def test_expired_events_are_being_deleted():
    expired = timezone.now() - datetime.timedelta(days=32)
    with freeze_time(expired):
        event = baker.make(Event, max_participants=1, start_date_time=timezone.now())
        baker.make(Registration, event=event)
        baker.make(WaitListEntry, event=event)
    cmd.handle()
    assert not Event.objects.exists()


@pytest.mark.django_db
def test_reminder_mails_for_future_deletion_are_being_sent():
    end = timezone.now() - datetime.timedelta(days=27)
    event = baker.make(Event, max_participants=1, end_date_time=end)
    baker.make(Registration, event=event)
    cmd.handle()
    assert "Reminder: These past events will soon be deleted" in mail.outbox[0].subject
    assert event.title in mail.outbox[0].body
