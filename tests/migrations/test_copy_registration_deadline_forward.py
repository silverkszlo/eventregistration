import datetime

import pytest
from django.utils import timezone


@pytest.mark.django_db
def test_copy_registration_deadline_forward_migration(migrator):
    start = timezone.now() + datetime.timedelta(days=15)
    end = timezone.now() + datetime.timedelta(days=17)
    registration_deadline = timezone.now() + datetime.timedelta(days=10)

    old_state = migrator.apply_initial_migration(('registration', '0026_remove_event_max_waitlist_spots'))
    Event = old_state.apps.get_model('registration', 'Event')

    Event.objects.create(
        start_date_time=start,
        end_date_time=end,
        max_participants=1,
        registration_deadline=registration_deadline
    )

    assert Event.objects.count() == 1
    assert Event.objects.filter(registration_deadline=registration_deadline).count() == 1

    new_state = migrator.apply_tested_migration(('registration', '0027_copy_registration_deadline_forward'))
    Event = new_state.apps.get_model('registration', 'Event')

    assert Event.objects.count() == 1
    assert Event.objects.filter(
        signup_deadline=registration_deadline,
        signoff_deadline=registration_deadline
    ).count() == 1

    migrator.reset()
