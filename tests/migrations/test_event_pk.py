import datetime

import pytest
from django.utils import timezone


@pytest.mark.django_db
def test_event_pk_migration(migrator):
    old_state = migrator.apply_initial_migration(('registration', '0030_waitlistentry_newsletter'))
    Event = old_state.apps.get_model('registration', 'Event')
    Registration = old_state.apps.get_model('registration', 'Registration')
    WaitListEntry = old_state.apps.get_model('registration', 'WaitListEntry')

    event = Event.objects.create(
        title="Event",
        start_date_time=timezone.now() + datetime.timedelta(days=15),
        end_date_time=timezone.now() + datetime.timedelta(days=17),
        max_participants=1,
        signoff_deadline=timezone.now() + datetime.timedelta(days=9),
        signup_deadline=timezone.now() + datetime.timedelta(days=10),
    )
    another_event = Event.objects.create(
        title="Another event",
        start_date_time=timezone.now() + datetime.timedelta(days=15),
        end_date_time=timezone.now() + datetime.timedelta(days=17),
        max_participants=1,
        signoff_deadline=timezone.now() + datetime.timedelta(days=9),
        signup_deadline=timezone.now() + datetime.timedelta(days=10),
    )
    Registration.objects.create(
        event=event,
        participant_name="Testname",
        email_address="test@example.com",
        order_id="unique_string_1",
    )
    Registration.objects.create(
        event=another_event,
        participant_name="AnotherTestname",
        email_address="test@example.com",
        order_id="unique_string_2",
    )
    WaitListEntry.objects.create(
        event=event,
        participant_name="YetAnotherTestname",
        email_address="test@example.com",
    )
    assert Event.objects.count() == 2

    new_state = migrator.apply_tested_migration(('registration', '0031_event_pk'))
    Event = new_state.apps.get_model('registration', 'Event')
    event = Event.objects.get(title="Event")
    another_event = Event.objects.get(title="Another event")
    assert event.registrations.first().participant_name == "Testname"
    assert event.waitlist_entries.first().participant_name == "YetAnotherTestname"
    assert another_event.registrations.first().participant_name == "AnotherTestname"

    migrator.reset()
