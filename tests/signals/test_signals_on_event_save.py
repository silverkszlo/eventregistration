from django.utils import timezone
from freezegun import freeze_time


def test_event_save_reminds_waitlist(client, future_event_one_seat, registration, confirmed_waitlistentry):
    now = timezone.now()
    with freeze_time(now):
        future_event_one_seat.max_participants = 2
        future_event_one_seat.save()
    confirmed_waitlistentry.refresh_from_db()
    assert confirmed_waitlistentry.reminded_at == now
