from django.core import mail
from django.utils import timezone
from freezegun import freeze_time


def test_registration_guestlist_reminds_waitlist(
    client, future_event_one_seat, registration, confirmed_waitlistentry
):
    assert not confirmed_waitlistentry.reminded_at

    now = timezone.now()
    with freeze_time(now):
        registration.guestlist = True
        registration.save()

    confirmed_waitlistentry.refresh_from_db()
    assert confirmed_waitlistentry.reminded_at == now
    assert len(mail.outbox) == 1
