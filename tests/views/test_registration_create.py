import datetime

import pytest
from django.core import mail
from django.utils import timezone
from freezegun import freeze_time
from model_bakery import baker

from registration.models import Event, Registration, WaitListEntry


def test_registration_create_view(client, future_event_one_seat, confirmed_reminded_waitlistentry):
    url = '/registration/{}/'.format(confirmed_reminded_waitlistentry.uuid)
    response = client.get(url)
    assert response.status_code == 200


def test_registration_create_post(client, future_event_one_seat, confirmed_reminded_waitlistentry):
    url = '/registration/{}/'.format(confirmed_reminded_waitlistentry.uuid)
    response = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })

    assert response.status_code == 302


def test_registration_create_only_works_for_unreminded_wles_if_event_open(
    client, future_event_one_seat, registration, confirmed_waitlistentry
):
    url = '/registration/{}/'.format(confirmed_waitlistentry.uuid)
    response = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    assert future_event_one_seat.status == Event.WAITLIST
    assert response.status_code == 302
    assert Registration.objects.count() == 1


def test_registration_create_only_works_for_confirmed_wles(
    client, future_event_one_seat, unconfirmed_waitlistentry
):
    """
    Test that the RegistrationCreate view is inaccessible if the corresponding WaitListEntry
    is unconfirmed.
    """
    url = '/registration/{}/'.format(unconfirmed_waitlistentry.uuid)
    response = client.get(url)
    assert response.status_code == 404
    assert future_event_one_seat.status == Event.OPEN
    assert not Registration.objects.exists()


@pytest.mark.django_db
def test_registration_create_multiple_signups_with_same_email_registration(client):
    """
    Test that a second registration can't be created if there's already one with
    the same email address.
    """
    start_date_time = timezone.now() + datetime.timedelta(days=10)
    event = baker.make(Event, max_participants=2, start_date_time=start_date_time)
    with freeze_time("2000-01-01 12:00"):
        registration = baker.make(Registration, event=event)
    reminded_waitlist_entry = baker.make(
        WaitListEntry,
        event=event,
        confirmed=True,
        reminded_at=timezone.now(),
        email_address=registration.email_address
    )
    url = '/registration/{}/'.format(reminded_waitlist_entry.uuid)
    client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })

    assert Registration.objects.filter(email_address=registration.email_address).count() == 1


@pytest.mark.django_db
def test_registration_allows_same_email_as_waitlist(
    client, future_event_one_seat, confirmed_reminded_waitlistentry
):
    url = '/registration/{}/'.format(confirmed_reminded_waitlistentry.uuid)
    client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    assert Registration.objects.count() == 1


def test_registration_create_deadline(client, future_event_one_seat, confirmed_reminded_waitlistentry):
    future_event_one_seat.signup_deadline = timezone.now()
    future_event_one_seat.save()
    url = '/registration/{}/'.format(confirmed_reminded_waitlistentry.uuid)
    response = client.get(url)
    assert response.status_code == 302


def test_registration_create_deadline_post(
    client, future_event_one_seat, confirmed_reminded_waitlistentry
):
    future_event_one_seat.signup_deadline = timezone.now()
    future_event_one_seat.save()
    url = '/registration/{}/'.format(confirmed_reminded_waitlistentry.uuid)
    response = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    assert future_event_one_seat.status == Event.CLOSED
    assert response.status_code == 302
    assert not Registration.objects.exists()


@pytest.mark.django_db
def test_registration_inaccessible_after_event_start(client, future_event_one_seat):
    now = timezone.now()
    later = timezone.now()
    future_event_one_seat.start_date_time = now
    future_event_one_seat.signup_deadline = later
    future_event_one_seat.max_participants = 1
    future_event_one_seat.save()
    reminded_waitlist_entry = baker.make(
        WaitListEntry,
        event=future_event_one_seat,
        confirmed=True,
        reminded_at=timezone.now(),
    )
    url = '/registration/{}/'.format(reminded_waitlist_entry.uuid)

    get_response = client.get(url)
    post_response = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })

    assert future_event_one_seat.status == Event.OVER
    assert get_response.status_code == 302
    assert post_response.status_code == 302
    assert not Registration.objects.exists()


@pytest.mark.django_db
def test_only_one_registration_per_waitlistentry(client):
    """
    Test that only one Registration can be created per WaitListEntry
    """
    start_date_time = timezone.now() + datetime.timedelta(days=10)
    signup_deadline = timezone.now() + datetime.timedelta(days=5)
    event = baker.make(
        Event,
        max_participants=2,
        start_date_time=start_date_time,
        signup_deadline=signup_deadline,
    )

    with freeze_time("2000-01-01 12:00"):
        registration = baker.make(Registration, event=event)
        waitlistentry = baker.make(WaitListEntry, event=event, confirmed=True, uuid=registration.uuid)

    url = '/registration/{}/'.format(waitlistentry.uuid)
    client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })

    assert event.status == Event.OPEN
    assert Registration.objects.count() == 1


@pytest.mark.django_db
def test_no_reminder_mail_for_wle_on_registration_create(client):
    """
    Test that creating Registration doesn't send a reminder to same WaitListEntry
    """
    start_date_time = timezone.now() + datetime.timedelta(days=10)
    signup_deadline = timezone.now() + datetime.timedelta(days=5)
    event = baker.make(
        Event,
        max_participants=2,
        start_date_time=start_date_time,
        signup_deadline=signup_deadline,
    )

    waitlistentry = baker.make(WaitListEntry, event=event, confirmed=True)

    participant_name = 'Testname'
    email_address = 'somebody@example.com'

    # create registration from waitlistentry
    url = '/registration/{}/'.format(waitlistentry.uuid)
    client.post(url, {
        'participant_name': participant_name,
        'email_address': email_address,
    })

    assert len(mail.outbox) == 1


@pytest.mark.django_db
def test_paid_confirmation_mail_for_paid_event(client):
    """
    Test that creating Registration for paid event sends the correct email
    """
    start_date_time = timezone.now() + datetime.timedelta(days=10)
    signup_deadline = timezone.now() + datetime.timedelta(days=5)
    event = baker.make(
        Event,
        max_participants=2,
        start_date_time=start_date_time,
        signup_deadline=signup_deadline,
        low_fee=10,
        medium_fee=20,
        high_fee=30,
    )

    waitlistentry = baker.make(WaitListEntry, event=event, confirmed=True)

    participant_name = 'Testname'
    email_address = 'somebody@example.com'

    # create registration from waitlistentry
    url = '/registration/{}/'.format(waitlistentry.uuid)
    client.post(url, {
        'participant_name': participant_name,
        'email_address': email_address,
        'participation_fee': 20,
    })

    assert "when transferring the participation fee" in mail.outbox[0].body


@pytest.mark.django_db
def test_free_confirmation_mail_for_free_event(client):
    """
    Test that creating Registration for free event sends the correct email
    """
    start_date_time = timezone.now() + datetime.timedelta(days=10)
    signup_deadline = timezone.now() + datetime.timedelta(days=5)
    event = baker.make(
        Event,
        max_participants=2,
        start_date_time=start_date_time,
        signup_deadline=signup_deadline,
    )

    waitlistentry = baker.make(WaitListEntry, event=event, confirmed=True)

    participant_name = 'Testname'
    email_address = 'somebody@example.com'

    # create registration from waitlistentry
    url = '/registration/{}/'.format(waitlistentry.uuid)
    client.post(url, {
        'participant_name': participant_name,
        'email_address': email_address,
    })

    assert "when transferring the participation fee" not in mail.outbox[0].body


@pytest.mark.django_db
def test_participation_fee_works(client):
    start_date_time = timezone.now() + datetime.timedelta(days=10)
    signup_deadline = timezone.now() + datetime.timedelta(days=5)
    event = baker.make(
        Event,
        max_participants=2,
        start_date_time=start_date_time,
        signup_deadline=signup_deadline,
        low_fee=10,
        medium_fee=20,
        high_fee=30,
    )

    waitlistentry = baker.make(WaitListEntry, event=event, confirmed=True)

    # create registration from waitlistentry
    url = '/registration/{}/'.format(waitlistentry.uuid)
    client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
        'participation_fee': 10,
    })

    assert Registration.objects.filter(
        participant_name='Testname',
        participation_fee=10,
        event=event,
        uuid=waitlistentry.uuid,
    ).exists()


@pytest.mark.django_db
def test_participation_fee_required_for_paid_event(client):
    start_date_time = timezone.now() + datetime.timedelta(days=10)
    signup_deadline = timezone.now() + datetime.timedelta(days=5)
    event = baker.make(
        Event,
        max_participants=2,
        start_date_time=start_date_time,
        signup_deadline=signup_deadline,
        low_fee=10,
        medium_fee=20,
        high_fee=30,
    )

    waitlistentry = baker.make(WaitListEntry, event=event, confirmed=True)

    # create registration from waitlistentry
    url = '/registration/{}/'.format(waitlistentry.uuid)
    client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })

    assert not Registration.objects.filter(participant_name='Testname').exists()


@pytest.mark.django_db
def test_participation_fee_must_be_one_of_the_choices(client):
    start_date_time = timezone.now() + datetime.timedelta(days=10)
    signup_deadline = timezone.now() + datetime.timedelta(days=5)
    event = baker.make(
        Event,
        max_participants=2,
        start_date_time=start_date_time,
        signup_deadline=signup_deadline,
        low_fee=10,
        medium_fee=20,
        high_fee=30,
    )

    waitlistentry = baker.make(WaitListEntry, event=event, confirmed=True)

    # create registration from waitlistentry
    url = '/registration/{}/'.format(waitlistentry.uuid)
    client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
        'participation_fee': 5,
    })

    assert not Registration.objects.filter(participant_name='Testname').exists()
