import pytest
from django.conf import settings
from django.core import mail
from model_bakery import baker

from registration.models import WaitListEntry


@pytest.mark.django_db
def test_send_newsletter_subscription_mail(client, future_event_one_seat):
    waitlistentry = baker.make(WaitListEntry, newsletter=True, event=future_event_one_seat)
    url = '/confirm/{}/'.format(waitlistentry.uuid)
    response = client.get(url)

    waitlistentry.refresh_from_db()
    assert waitlistentry.newsletter is False
    assert response.status_code == 302
    assert len(mail.outbox) == 1
    assert settings.NEWSLETTER_SIGN_UP_SUBJECT == mail.outbox[0].subject
    assert waitlistentry.email_address.lower() in mail.outbox[0].body


@pytest.mark.django_db
def test_old_confirmlink_still_works(client, future_event_one_seat, unconfirmed_waitlistentry):
    old_url = '/waitlistentry/confirm/{}/'.format(unconfirmed_waitlistentry.uuid)
    response = client.get(old_url)
    assert response.status_code == 302
    unconfirmed_waitlistentry.refresh_from_db()
    assert unconfirmed_waitlistentry.confirmed is True


@pytest.mark.django_db
def test_waitlistentry_confirm_view(client, future_event_one_seat, unconfirmed_waitlistentry):
    url = '/waitlistentry/confirm/{}/'.format(unconfirmed_waitlistentry.uuid)
    response = client.get(url)
    assert response.status_code == 302
    unconfirmed_waitlistentry.refresh_from_db()
    assert unconfirmed_waitlistentry.confirmed is True
