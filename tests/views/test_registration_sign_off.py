import datetime

import pytest
from django.conf import settings
from django.core import mail
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time
from model_bakery import baker

from registration.models import Event, Registration, WaitListEntry


def test_signing_off_within_free_cancellation_period(client, future_event_one_seat, registration):
    client.post(reverse('registration-sign-off', args=[registration.uuid]))
    assert not Registration.objects.exists()


def test_signing_off_outside_free_cancellation_period(client, future_event_one_seat, registration):
    future_event_one_seat.signoff_deadline = timezone.now() - datetime.timedelta(days=1)
    future_event_one_seat.save()
    client.post(reverse('registration-sign-off', args=[registration.uuid]))
    registration.refresh_from_db()
    assert registration.signed_off is True


@pytest.mark.django_db
def test_registration_sign_off_triggers_waitlist_reminder(client):
    start = timezone.now() + datetime.timedelta(days=10)
    later = timezone.now() + datetime.timedelta(days=8)
    event = baker.make(
        Event,
        max_participants=2,
        signup_deadline=later,
        start_date_time=start,
    )

    now = timezone.now()
    with freeze_time(now):
        registration = baker.make(Registration, event=event)
        unreminded_waitlist_entry = baker.make(
            WaitListEntry,
            event=event,
            confirmed=True,
        )
        url = '/registration/sign-off/{}/'.format(registration.uuid)
        response = client.post(url)
        unreminded_waitlist_entry.refresh_from_db()
        assert response.status_code == 302
        assert unreminded_waitlist_entry.reminded_at == now
        assert len(mail.outbox) == 1


@pytest.mark.django_db
def test_registration_sign_off_reminds_as_many_as_needed(client):
    now = timezone.now()
    with freeze_time(now):
        start_date_time = timezone.now() + datetime.timedelta(days=10)
        event = baker.make(Event, max_participants=2, start_date_time=start_date_time)
        registration_1 = baker.make(Registration, event=event)
        registration_2 = baker.make(Registration, event=event)
        unreminded_waitlist_entry_1 = baker.make(
            WaitListEntry,
            event=event,
            confirmed=True,
        )
        unreminded_waitlist_entry_2 = baker.make(
            WaitListEntry,
            event=event,
            confirmed=True,
        )

        response_1 = client.post('/registration/sign-off/{}/'.format(registration_1.uuid))
        response_2 = client.post('/registration/sign-off/{}/'.format(registration_2.uuid))
        unreminded_waitlist_entry_1.refresh_from_db()
        unreminded_waitlist_entry_2.refresh_from_db()
        assert response_1.status_code == 302
        assert response_2.status_code == 302
        assert unreminded_waitlist_entry_1.reminded_at == now
        assert unreminded_waitlist_entry_2.reminded_at == now
        assert len(mail.outbox) == 2


@pytest.mark.django_db
def test_registration_sign_off_rereminds_already_reminded(client):
    start = timezone.now() + datetime.timedelta(days=10)
    later = timezone.now() + datetime.timedelta(days=8)
    event = baker.make(
        Event,
        max_participants=2,
        signup_deadline=later,
        start_date_time=start,
    )

    now = timezone.now()
    with freeze_time(now):
        registration = baker.make(Registration, event=event)
        reminded_waitlist_entry = baker.make(
            WaitListEntry,
            event=event,
            confirmed=True,
            reminded_at=now,
        )
        unreminded_waitlist_entry = baker.make(
            WaitListEntry,
            event=event,
            confirmed=True,
        )
    later = timezone.now() + datetime.timedelta(days=5)
    with freeze_time(later):
        url = '/registration/sign-off/{}/'.format(registration.uuid)
        response = client.post(url)
        reminded_waitlist_entry.refresh_from_db()
        unreminded_waitlist_entry.refresh_from_db()
        assert response.status_code == 302
        assert reminded_waitlist_entry.reminded_at == later
        assert unreminded_waitlist_entry.reminded_at == later
        assert len(mail.outbox) == 2


def test_sign_off_after_deadline_triggers_info_mail(
    client, future_event_one_seat, registration, confirmed_waitlistentry
):
    after_deadline = timezone.now() + datetime.timedelta(days=9)
    with freeze_time(after_deadline):
        url = '/registration/sign-off/{}/'.format(registration.uuid)
        response = client.post(url)

    registration.refresh_from_db()
    assert response.status_code == 302
    assert registration.signed_off is True
    assert len(mail.outbox) == 1
    subject = settings.ORGANIZER_SIGN_OFF_MAIL_SUBJECT.format(
        event=future_event_one_seat.title, date=future_event_one_seat.start_date_time.date()
    )
    assert subject == mail.outbox[0].subject


def test_signoff_doesnt_remind_too_many_waitlist_entries(
    client, future_event_one_seat, registration, confirmed_waitlistentry
):
    confirmed_waitlistentry_2 = baker.make(WaitListEntry, event=future_event_one_seat, confirmed=True)

    # registration signs off
    url = '/registration/sign-off/{}/'.format(registration.uuid)
    client.post(url)
    assert not Registration.objects.exists()
    confirmed_waitlistentry.refresh_from_db()
    assert confirmed_waitlistentry.reminded_at and not confirmed_waitlistentry_2.reminded_at

    # confirmed_waitlistentry makes a registration
    url = '/registration/{}/'.format(confirmed_waitlistentry.uuid)
    client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'test@example.com',
    })

    sign_up_link = 'registration/{}/'.format(confirmed_waitlistentry.uuid)

    assert Registration.objects.filter(uuid=confirmed_waitlistentry.uuid).exists()
    assert not WaitListEntry.objects.filter(uuid=confirmed_waitlistentry.uuid).exists()
    assert len(mail.outbox) == 2
    assert sign_up_link in mail.outbox[0].body
    assert not confirmed_waitlistentry_2.reminded_at
