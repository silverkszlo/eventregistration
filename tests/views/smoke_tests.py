import pytest


@pytest.mark.django_db
def test_200_root(client):
    """
    Succesfully load index page.
    """
    url = ''
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_200_event_list(client):
    """
    Succesfully load event list page.
    """
    url = '/events/'
    response = client.get(url)
    assert response.status_code == 200


def test_404_view(client):
    response = client.get('registration/404.html')
    assert response.status_code == 404
