from datetime import datetime

import pytest
from django.core import mail
from django.utils import timezone
from faker import Faker
from freezegun import freeze_time
from model_bakery import baker

from registration.models import Event, WaitListEntry


def test_200_waitlistentry_create(client, future_event_one_seat, registration):
    url = '/{}/'.format(future_event_one_seat.pk)
    response = client.get(url)
    assert response.status_code == 200


def test_waitlistentry_create_post(client, future_event_one_seat, registration):
    url = '/{}/'.format(future_event_one_seat.pk)
    response = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    waitlistentry = WaitListEntry.objects.get(email_address='somebody@example.com')
    confirmlink = 'confirm/{}/'.format(waitlistentry.pk)
    assert response.status_code == 302
    assert confirmlink in mail.outbox[0].body


def test_waitlistentry_create_deadline(client, future_event_one_seat, registration):
    future_event_one_seat.signup_deadline = timezone.now()
    future_event_one_seat.save()
    url = '/{}/'.format(future_event_one_seat.pk)
    response = client.get(url)
    assert response.status_code == 200


def test_waitlistentry_create_deadline_post(client, future_event_one_seat, registration):
    future_event_one_seat.signup_deadline = timezone.now()
    future_event_one_seat.save()
    url = '/{}/'.format(future_event_one_seat.pk)
    response = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    assert future_event_one_seat.status == Event.CLOSED
    assert response.status_code == 302
    assert not WaitListEntry.objects.exists()


def test_waitlistentry_create_inaccessible_after_event_start(client, future_event_one_seat, registration):
    future_event_one_seat.start_date_time = timezone.now()
    future_event_one_seat.save()
    url = '/{}/'.format(future_event_one_seat.pk)
    get = client.get(url)
    post = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    assert future_event_one_seat.status == Event.OVER
    assert get.status_code == 302
    assert post.status_code == 302
    assert not WaitListEntry.objects.exists()


def test_waitlistentry_create_doesnt_work_for_closed_events(client, future_event_one_seat):
    future_event_one_seat.signup_deadline = timezone.now()
    future_event_one_seat.save()
    assert future_event_one_seat.status == Event.CLOSED

    url = '/{}/'.format(future_event_one_seat.pk)
    get = client.get(url)
    assert get.status_code == 200

    post = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    assert post.status_code == 302
    assert not WaitListEntry.objects.exists()


def test_captcha_used_when_necessary(client, future_event_one_seat):
    fake = Faker('de_DE')
    for i in range(21):
        baker.make(
            WaitListEntry,
            event=future_event_one_seat,
            confirmed=False,
            participant_name=fake.name(),
            email_address=fake.email(),
            reminded_at=None,
        )

    url = '/{}/'.format(future_event_one_seat.pk)

    post = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    assert post.status_code == 200
    assert not WaitListEntry.objects.filter(
        participant_name='Testname'
    ).exists()


def test_captcha_not_used_when_unnecessary(client, future_event_one_seat):
    fake = Faker('de_DE')
    for i in range(20):
        baker.make(
            WaitListEntry,
            event=future_event_one_seat,
            confirmed=False,
            participant_name=fake.name(),
            email_address=fake.email(),
            reminded_at=None,
        )

    url = '/{}/'.format(future_event_one_seat.pk)

    post = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    assert post.status_code == 302
    assert WaitListEntry.objects.filter(
        participant_name='Testname'
    ).exists()


@pytest.mark.django_db
def test_old_wles_not_counted_for_captcha(client):
    event = baker.make(
        Event,
        start_date_time=timezone.make_aware(datetime(2023, 12, 24, 12, 00)),
        max_participants=1,
        signup_deadline=timezone.make_aware(datetime(2023, 12, 23, 12, 00)),
        signoff_deadline=timezone.make_aware(datetime(2023, 12, 22, 12, 00)),
    )
    with freeze_time("2023-12-01 12:00"):
        fake = Faker('de_DE')
        for i in range(21):
            baker.make(
                WaitListEntry,
                event=event,
                confirmed=False,
                participant_name=fake.name(),
                email_address=fake.email(),
                reminded_at=None,
            )

    url = '/{}/'.format(event.pk)

    with freeze_time("2023-12-02 13:00"):
        post = client.post(url, {
            'participant_name': 'Testname',
            'email_address': 'somebody@example.com',
        })

    assert post.status_code == 302
    assert WaitListEntry.objects.filter(
        participant_name='Testname'
    ).exists()


@pytest.mark.django_db
def test_wle_create_email_already_used(client, future_event_one_seat):
    """
    Test that a second WLE can't be created for the same email address.
    """
    with freeze_time("2000-01-01 12:00"):
        baker.make(WaitListEntry, event=future_event_one_seat, email_address="somebody@example.com", confirmed=True)
    url = '/{}/'.format(future_event_one_seat.pk)
    response = client.post(url, {
        'participant_name': 'Testname',
        'email_address': 'somebody@example.com',
    })
    assert response.status_code == 302
    assert (
        'Your email address has already been used to sign up for the event' in mail.outbox[0].subject
    )
    assert WaitListEntry.objects.filter(email_address="somebody@example.com").count() == 1


@pytest.mark.django_db
def test_wle_create_with_url_in_name(client, future_event_one_seat):
    """
    Test that WLE's with http:// or https:// as participant_name can't be created.
    """
    url = '/{}/'.format(future_event_one_seat.pk)
    response = client.post(url, {
        'participant_name': 'http://dodgyurl.com',
        'email_address': 'somebody@example.com',
    })
    assert response.status_code == 302
    assert not WaitListEntry.objects.filter(email_address="somebody@example.com").exists()

    response = client.post(url, {
        'participant_name': 'https://dodgyurl.com',
        'email_address': 'somebody2@example.com',
    })
    assert response.status_code == 302
    assert not WaitListEntry.objects.filter(email_address="somebody2@example.com").exists()
