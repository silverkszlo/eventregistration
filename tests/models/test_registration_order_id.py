import datetime

import pytest
from django.core import mail
from model_bakery import baker

from registration.models import Event, Registration, WaitListEntry


def test_order_id_in_confirmation_email_and_in_database(client, future_event_one_seat):
    confirmed_waitlist_entry = baker.make(
        WaitListEntry,
        event=future_event_one_seat,
        confirmed=True,
    )
    participant_name = 'Testname'
    email_address = 'somebody@example.com'

    url = '/registration/{}/'.format(confirmed_waitlist_entry.uuid)
    client.post(url, {
        'participant_name': participant_name,
        'email_address': email_address,
    })

    registration = Registration.objects.get(email_address=email_address)
    assert Registration.objects.filter(order_id=registration.order_id).exists()
    assert registration.order_id in mail.outbox[0].body


@pytest.mark.django_db
def test_order_id_unchanged_after_event_update():
    event = baker.make(Event)
    registration = baker.make(Registration, event=event)
    initial_order_id = registration.order_id

    event.start_date_time = event.start_date_time + datetime.timedelta(days=3)
    event.save()

    registration.refresh_from_db()
    order_id_after_update = registration.order_id
    assert initial_order_id == order_id_after_update
