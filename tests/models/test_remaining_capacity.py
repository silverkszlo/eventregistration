from model_bakery import baker

from registration.models import Registration


def test_remaining_capacity_with_signed_off_registration(future_event_one_seat):
    baker.make(Registration, event=future_event_one_seat, signed_off=True)
    assert future_event_one_seat.remaining_capacity == 1


def test_remaining_capacity_with_confirmed_registration(future_event_one_seat, registration):
    assert future_event_one_seat.remaining_capacity == 0
