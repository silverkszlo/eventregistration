import datetime

import pytest
from django.utils import timezone
from model_bakery import baker

from registration.models import Event, Registration, WaitListEntry


@pytest.mark.django_db
def test_email_already_used_property_works_registration(client):
    """
    Test that the email_already_used property works when creating a registration if a
    registration with the same email address already exists
    """
    event = Event.objects.create(
        start_date_time=timezone.make_aware(datetime.datetime(2023, 12, 24, 12, 00)),
        end_date_time=timezone.make_aware(datetime.datetime(2023, 12, 25, 12, 00)),
        max_participants=1,
        signup_deadline=timezone.make_aware(datetime.datetime(2023, 12, 23, 12, 00)),
        signoff_deadline=timezone.make_aware(datetime.datetime(2023, 12, 22, 12, 00)),
    )
    Registration.objects.create(
        participant_name='Testname', email_address="test@example.com", event=event, order_id="unique_string_1",
    )
    second_registration = Registration(
        participant_name='Testname', email_address="test@example.com", event=event, order_id="unique_string_2",
    )
    assert second_registration.email_already_used


@pytest.mark.django_db
def test_email_already_used_property_works_confirmed_wle(client):
    """
    Test that the email_already_used property works when creating a registration if a
    WaitListEntry with the same email address already exists
    """
    start = timezone.now() + datetime.timedelta(days=10)
    later = timezone.now() + datetime.timedelta(days=8)
    before = timezone.now() + datetime.timedelta(days=3)
    event = baker.make(
        Event,
        start_date_time=start,
        max_participants=2,
        signup_deadline=later,
        signoff_deadline=before,
    )
    WaitListEntry.objects.create(
        participant_name='Testname', email_address="test@example.com", event=event, confirmed=True
    )
    registration = Registration(
        participant_name='Testname', email_address="test@example.com", event=event
    )
    assert registration.email_already_used


@pytest.mark.django_db
def test_email_already_used_property_doesnt_trigger_for_unconfirmed_wle(client):
    """
    Test that the email_already_used property doesn't trigger when creating a
    registration if an unconfirmed WaitListEntry with the same email address already
    exists
    """
    start = timezone.now() + datetime.timedelta(days=10)
    later = timezone.now() + datetime.timedelta(days=8)
    before = timezone.now() + datetime.timedelta(days=3)
    event = baker.make(
        Event,
        start_date_time=start,
        max_participants=2,
        signup_deadline=later,
        signoff_deadline=before,
    )
    WaitListEntry.objects.create(
        participant_name='Testname', email_address="test@example.com", event=event, confirmed=False
    )
    registration = Registration(
        participant_name='Testname', email_address="test@example.com", event=event
    )
    assert not registration.email_already_used


@pytest.mark.django_db
def test_email_already_used_property_doesnt_trigger_for_empty_event(client):
    start = timezone.now() + datetime.timedelta(days=10)
    later = timezone.now() + datetime.timedelta(days=8)
    before = timezone.now() + datetime.timedelta(days=3)
    event = baker.make(
        Event,
        start_date_time=start,
        max_participants=2,
        signup_deadline=later,
        signoff_deadline=before,
    )
    registration = Registration(
        participant_name='Testname', email_address="test@example.com", event=event
    )
    assert not registration.email_already_used
