"""
Django base settings for registrationform project.
Development and production settings inherit from this file.
"""

import datetime
from pathlib import Path

from django.contrib.messages import constants as messages

EVR_VERSION = '1.4.8'

BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bootstrap4',
    'registration',
    'import_export',
    'captcha',
]

# default value is False. setting value to True determines
# that the library will use database transactions on data import
IMPORT_EXPORT_USE_TRANSACTIONS = True

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'eventregistration.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'eventregistration.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'de'

LANGUAGES = (
    ('de', ('German')),
    ('en', ('English')),
)

TIME_ZONE = 'Europe/Berlin'

USE_I18N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / "static"

MESSAGE_TAGS = {
    messages.DEBUG: 'alert-info',
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-danger',
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# flite is used to create the .wav files for visually impaired users
CAPTCHA_FLITE_PATH = '/usr/bin/flite'

REGISTRATION_DELETION_PERIOD = datetime.timedelta(days=30)

WAITLIST_REMINDER_PERIOD = datetime.timedelta(hours=24)

REGISTRATION_DELETION_MEDIUM_REMAINING_PERIOD = datetime.timedelta(days=7)

REGISTRATION_DELETION_SMALL_REMAINING_PERIOD = datetime.timedelta(days=3)

ORGANIZER_CONTACT_EMAIL = 'info@educat-kollektiv.org'

ADMIN_EMAIL = 'test@example.com'

REGISTRATION_DELETION_REMINDER_MAIL_SUBJECT = (
    'Erinnerung: Diese vergangenen Veranstaltungen werden bald gelöscht '
    '/ Reminder: These past events will soon be deleted'
)

REGISTRATION_DELETION_REMINDER_MAIL_BODY = (
    "Liebe*r Organisator*in,\n\n"
    "die folgenden Veranstaltungen sind vorbei und die zugehörigen Anmeldungsdaten "
    "werden in {deletion_period_small} Tag(en) automatisch und endgültig gelöscht:\n\n"
    "{high_priority_events}\n\n"

    "Um die Daten zu sichern, lade sie bitte unbedingt vorher herunter!\n\n"
    "Die folgenden Veranstaltungen und die zugehörigen Anmeldungsdaten werden "
    "in {deletion_period_medium} Tagen gelöscht:\n\n"
    "{medium_priority_events}\n\n"
    "Du erhältst diese Erinnerung solange noch Anmeldungsdaten für eine Veranstaltung vorhanden sind.\n"
    "Nachdem du die Daten gesichert hast, kannst du die Anmeldungen oder auch die entsprechende Veranstaltung "
    "im Admin Interface löschen.\n"
    "Danach erhältst du keine weiteren Erinnerungen für diese Veranstaltung."

    "\n\n\n---\n\n\n"

    "Dear organizer,\n\n"
    "the following events are over and the corresponding registration data "
    "will be automatically and finally deleted in {deletion_period_small} day(s):\n\n"
    "{high_priority_events}\n\n"
    "Please make sure to download all the registration data before.\n\n"

    "The following events and the corresponding data will be deleted in {deletion_period_medium} days:\n\n"
    "{medium_priority_events}\n\n"
    "You will receive this notification as long as there are still registrations for this the event.\n"
    "After saving the registration data, you can delete the remaining registrations or the respective event "
    "in the admin interface and you will not receive further notifications about that event.\n"
)

EVR_FINAL_CONFIRMATION_MAIL_BODY_PAID_EVENT = (
    "Hey {name},\n\n"
    "deine Anmeldung für die Veranstaltung {event} am {date} war erfolgreich.\n"
    "Deine Anmeldungs-ID ist:\n\n{order_id}\n\n"
    "Bitte behalte diese Mail, damit du uns deine Anmeldungs-ID mitteilen kannst, falls notwendig.\n\n"
    "Bitte gib die Anmeldungs-ID unbedingt als Verwendungszweck der Überweisung an. "
    "Beachte auch, dass du gegebenenfalls einen Teil des Beitrags zahlen musst, falls du dich erst "
    "nach Ablauf der Abmeldefrist am {signoff_deadline} abmelden solltest.\n\n"
    "Wenn du feststellst, dass du nicht an der Veranstaltung teilnehmen kannst, "
    "würden wir dich bitten, dich über den folgenden Link wieder abzumelden um den Platz für die nächste Person freizugeben:\n\n"
    "{delete_link}\n\n"
    "Wir freuen uns auf deine Teilnahme.\n"
    "Bis bald!\n\n"
    "Bitte nicht auf diese E-Mail antworten! Bei Fragen bezüglich der Veranstaltung meldet euch an: "
    + ORGANIZER_CONTACT_EMAIL +

    "\n\n\n---\n\n\n"

    "Hey {name},\n\n"
    "your registration for the event {event} on {date} was successful.\n"
    "Your registration ID is: {order_id}\n"
    "Please keep this email so that you can tell us your registration ID if necessary.\n\n"
    "Please use your registration ID as note to payee when transferring the participation fee. "
    "Keep in mind that you might have to pay "
    "a part of the fee if you sign off after the end of the free cancellation period on {signoff_deadline}.\n\n"
    "If you realize that you cannot participate in the event, "
    "we kindly ask you to sign off in order to free the seat for the next person by following this link:\n\n{delete_link}\n\n"
    "We are looking forward to your participation.\n"
    "See you soon!\n\n"
    "Please don't answer to this e-mail. If you have any questions concerning the event, please write to: "
    + ORGANIZER_CONTACT_EMAIL + "\n"
)

EVR_FINAL_CONFIRMATION_MAIL_BODY_FREE_EVENT = (
    "Hey {name},\n\n"
    "deine Anmeldung für die Veranstaltung {event} am {date} war erfolgreich.\n"
    "Deine Anmeldungs-ID ist:\n\n{order_id}\n\n"
    "Bitte behalte diese Mail, damit du uns deine Anmeldungs-ID mitteilen kannst, falls notwendig.\n\n"
    "Wenn du feststellst, dass du nicht an der Veranstaltung teilnehmen kannst, "
    "würden wir dich bitten, dich über den folgenden Link wieder abzumelden um den Platz für die nächste Person freizugeben:\n\n"
    "{delete_link}\n\n"
    "Wir freuen uns auf deine Teilnahme.\n"
    "Bis bald!\n\n"
    "Bitte nicht auf diese E-Mail antworten! Bei Fragen bezüglich der Veranstaltung meldet euch an: "
    + ORGANIZER_CONTACT_EMAIL +

    "\n\n\n---\n\n\n"

    "Hey {name},\n\n"
    "your registration for the event {event} on {date} was successful.\n"
    "Your registration ID is: {order_id}\n"
    "Please keep this email so that you can tell us your registration ID if necessary.\n\n"
    "If you realize that you cannot participate in the event, "
    "we kindly ask you to sign off in order to free the seat for the next person by following this link:\n\n{delete_link}\n\n"
    "We are looking forward to your participation.\n"
    "See you soon!\n\n"
    "Please don't answer to this e-mail. If you have any questions concerning the event, please write to: "
    + ORGANIZER_CONTACT_EMAIL + "\n"
)

EVR_EMAIL_CONFIRMATION_MAIL_BODY = (
    "Hey {name},\n\n"
    "wir freuen uns, dass du dich für die Veranstaltung {event} am {date} interessierst.\n\n"
    "Bitte bestätige deine E-Mail-Adresse indem du auf folgenden Link klickst:\n\n{confirmlink}\n\n\n"

    "Wenn es noch freie Plätze für die Veranstaltung gibt, wirst du über den Link "
    "auf das endgültige Anmeldeformular weitergeleitet.\n"
    "Sollte es keinen freien Platz mehr geben, wirst du stattdessen automatisch auf die Warteliste gesetzt "
    "und benachrichtigt, sobald ein Platz frei wird.\n\n"

    "Falls du nicht bestätigst, werden deine Daten gelöscht sobald die Veranstaltung beginnt. "
    "Klicke hier um stattdessen deine Daten sofort zu löschen:\n\n{deletelink}\n\n"
    "Bitte nicht auf diese E-Mail antworten! Bei Fragen bezüglich der Veranstaltung melde dich an: "
    + ORGANIZER_CONTACT_EMAIL +

    "\n\n\n---\n\n\n"

    "Hey {name},\n\n"
    "we are pleased that you are interested in the event {event} on {date}.\n\n"
    "Please confirm your email address by clicking the following link:\n\n{confirmlink}\n\n\n"

    "If there are free seats at the event, the link will take you to the final registration form.\n"
    "If there are no free seats you will automatically be put on the waitlist instead "
    "and you will be notified via email when the next person signs off.\n\n"

    "If you do not confirm, your data will be deleted when the event starts. "
    "To delete it now, follow this link:\n\n{deletelink}\n\n"
    "Please don't answer to this e-mail. If you have any questions concerning the event, please write to: "
    + ORGANIZER_CONTACT_EMAIL + "\n"
)

EVR_EMAIL_ALREADY_USED_MAIL_BODY = (
    "Hey {name},\n\n"
    "du oder jemand anders hat deine E-Mail-Adresse verwendet, um sich für die Veranstaltung {event} "
    "am {date} anzumelden.\n"
    "Falls du dich anmelden möchtest, bestätige bitte deine alte Anmeldung.\n"
    "Du findest den Link in deinen E-Mails. Falls du dich nicht (erneut) für die Veranstaltung angemeldet hast, "
    "ignoriere bitte diese Mail.\n\n"
    "Bitte nicht auf diese E-Mail antworten! Bei Fragen bezüglich der Veranstaltung meldet euch an: "
    + ORGANIZER_CONTACT_EMAIL +

    "\n\n\n---\n\n\n"

    "Hey {name},\n\n"
    "you or someone else used your email address to sign up for the event {event} on {date}.\n"
    "However, this address has already been used for this purpose. "
    "If you'd like to sign up, please confirm your old registration in order to participate. "
    "You can find the link in your emails.\n"
    "If you didn't sign up for our event (again), please ignore this email.\n\n"
    "Please don't answer to this e-mail. If you have any questions concerning the event, please write to: "
    + ORGANIZER_CONTACT_EMAIL + "\n"
)

EVR_FREE_SPOT_ON_EVENT_MAIL_BODY = (
    "Hey {name},\n\n"
    "du stehst auf der Warteliste der Veranstaltung {event} am {date}.\n"
    "Da sich jemand abgemeldet hat, ist ein Platz frei geworden. "
    "Falls du noch teilnehmen möchtest, klicke bitte auf diesen Link:\n\n{signup_link}\n\n"
    "Du hast 12 Stunden Zeit bis wir der nächsten Person auf der Warteliste Bescheid geben.\n"
    "Falls du doch nicht teilnehmen willst klicke bitte auf diesen Link, damit wir die nächste Person "
    "auf der Warteliste unverzüglich benachrichtigen können:\n\n{deletelink}\n\n"
    "Vielen Dank!\n\n"
    "Bitte nicht auf diese E-Mail antworten! Bei Fragen bezüglich der Veranstaltung meldet euch an: "
    + ORGANIZER_CONTACT_EMAIL +

    "\n\n\n---\n\n\n"

    "Hey {name},\n\n"
    "you are on the waitlist for the event {event} on {date}.\n"
    "Someone has signed off from the event, so there is an available seat. "
    "If you still wish to participate, please follow this link:\n\n{signup_link}\n\n"
    "You have 12 hours before we notify the next person on the waitlist. "
    "If you don't want to participate in the event and give the next person "
    "the possibility to sign up right away, follow this link:\n\n{deletelink}\n\n"
    "Thank you!\n\n"
    "Please don't answer to this e-mail. If you have any questions concerning the event, please write to: "
    + ORGANIZER_CONTACT_EMAIL + "\n"
)

ORGANIZER_SIGN_OFF_MAIL_SUBJECT = (
    "Abmeldung von der Veranstaltung {event} am {date} nach Ende der Abmeldefrist"
    "/ Sign-off from the {event} on {date} after the end of the sign-off period"
)

ORGANIZER_SIGN_OFF_MAIL_BODY = (
    "Liebe Organisator*innen,\n\n"
    "Soeben hat sich eine angemeldete Teilnehmer:in der Veranstaltung {event} am {date} abgemeldet.\n"
    "Für die Veranstaltung sind noch {remaining_capacity_only_registrations} Plätze frei.\n"
    "Auf der Warteliste sind aktuell {waiting} Personen. Diese werden NICHT automatisch benachrichtigt, "
    "da die Anmeldefrist bereits vorbei ist."

    "\n\n\n---\n\n\n"

    "Dear organizers,\n\n"
    "a registered attendee for the event {event} on {date} has just signed off.\n"
    "There are still {remaining_capacity_only_registrations} spaces available for the event.\n"
    "There are currently {waiting} people on the waiting list. They will NOT be automatically notified "
    "as the registration period is already over.\n"
)

NEWSLETTER_SIGN_UP_SUBJECT = "Neue Anmeldung für den Newsletter / New newsletter subscription"

NEWSLETTER_SIGN_UP_BODY = (
    "Liebe Organisator*innen,\n\n"
    "soeben hat sich eine neue Person für den Newsletter angemeldet.\n"
    "Bitte fügt die folgende Email-Adresse hinzu:\n\n "
    "{email_address}"

    "\n\n\n---\n\n\n"

    "Dear organizers,\n\n"
    "a new person has just signed up for the newsletter.\n"
    "Please add the following email address:\n\n "
    "{email_address}"
)
