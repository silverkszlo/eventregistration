"""
Django development settings for EVR project.
"""

from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SECRET_KEY='django-insecure-!wo*h78gl1t3x=exnf2e1djx0f7lg!a2ax1p(hv=dd%8(g3db'

EVR_HOST_URL = 'http://127.0.0.1:8000'

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

# Email Backend
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

LANGUAGE_CODE = 'en'
