[[_TOC_]]

## EvR | Event Registration
EvR is a simple event management tool allowing organizers to create events and participants to
register for these or sign up for a waitlist.


## Framework
Built with
[Django - The web framework for perfectionists with deadlines](https://www.djangoproject.com/)


## Features
- Create, view, update and delete events
- Add customized questions to the registration form
to be ideally prepared for the specific needs and capabilities
of the event's participants
- Registration for participants
- Waitlist option for booked out events
- Newsletter signup for participants
- Automatic reminders for the waitlist once seats are freed
- send mails to participants from the admin interface
- RSS feed of events
- View and update payment information
- Export of registration data to mulitple file formats (csv, xlsx, ods)
- German translation available


## How to use?

**1. Login**

After creating a `superuser` (see [Installation](#installation)) and starting the server, you can
log in to the admin-site of the project to create new events or manage registrations and waitlist
entries.

**2. Users and Groups**

The admin-site has a built-in user and group management which allows you to manage credentials by
users and/or groups.

Just click on the `Add`-button for a user or a group, and name it. After saving, you can add and
remove credentials for e.g. adding, viewing, changing or deleting events etc.

**3. Event management**

After logging in, you will see a section on the left containing links to list views of
registrations, events, and waitlist entries.

**3.1 Add a new event**

To create a new event, click the `Add`-button next to the event link and fill out the form:

- Choose a title
- Add a number of maximum participants for the event
- Pick a start date and time, and an end date and time
- Pick a sign-up deadline until which sign-up will be possible
- Pick a sign-off deadline until which participants can sign-off free of charge
- Add a location for the event
- Optional: add one or more customized questions which you would like to ask
your participants so that you can prepare for special needs
(e.g. previous experience or skills but also allergies, physical constraints, etc.)
- click the `Save`-button

**3.2 View, update, and delete events**

To view a list of all events, click on the `Events` button in the left section.

You can select one or more events by clicking on their checkbox on the right.
For deleting the selected events, click on the dropdown menu on the top and
select the `Delete selected items`-option.

For updating an event, click on its title in the list.
The link will take you to a an editable detail view
of the event where you can commit your changes.

**4. Registration and waitlist management**

To view a list of all registrations or waitlist entries,
click on the respective link in the left section.

On the right side of the page, you will find a section containing
filters which allow you to filter by event and/or by payment status.

For editing, click on the participant name in the list,
this will take you to an editable detail view.

**5. Export registration data to spreadsheet**

EvR offers you an option to export registrations to a spreadsheet.
Available file formats are `.csv`, `.xlsx`, and `.ods`

To export data, go to the registrations list, optionally add a filter
(e.g. if you want to export registrations for a specific event),
and then click on the `Export`-button on the right side of the page.

You will be directed to a page where you can choose the file-format
for export. Launch the export by clicking the `Send`-button.

**6. Sign up for an event**

Participants who want to sign up for an event can do so by choosing
an event from the event list and click the `Request signup link`-button
after entering their name and email_address.

For events with free seats, participants are taken to the actual
registration form on confirmation of their email address.
For booked events, participants are automatically informed via email about
free seats.


## Installation

**Pre-requisites**

EvR requires Python > 3.8

**Get the source code by cloning the repository**

*a) clone with ssh*
```
git@gitlab.com:silverkszlo/eventregistration.git
```

*b) clone with https*
```
https://gitlab.com/silverkszlo/eventregistration.git
```


**Create and activate a virtual environment, e.g. [venv](https://docs.python.org/3/tutorial/venv.html)
for your project**
````
$ python3 -m venv myvenv
$ source myvenv/bin/activate
````

**Install packages**
````
apt install libz-dev libjpeg-dev libfreetype6-dev
$ python -m pip install --upgrade pip
$ pip install -r requirements.txt
````

**Apply migrations**
````
$ python manage.py migrate
````

**Create superuser**
````
$ python manage.py createsuperuser
````

**Start the server to run the application on localhost**
```
$ python manage.py runserver
```


## Extension: Custom commands
### Notifying people on the waitlist

The waitlist reminder consists of two different mechanisms:
Firstly, whenever someone signs off from an event, the first person on the waitlist gets reminded
automatically.
Secondly, when that person does then not sign up within a configurable timespan (by default 24
hours), the next person gets reminded. This second part happens through the command
`remind_waitlist`. It should be run every 2 hours:

```
# m h  dom mon dow   command
0 */2 * * * /home/mysite/venv/bin/python /home/mysite/mysite/manage.py remind_waitlist
```

### Reminding organizers about soon to be deleted expired data and deleting data

The command `cleanup_and_remind` informs the organizers about the imminent deletion of
soon-to-expire participant data and deletes expired data. The deletion happens 30 days after the end
of the event. The reminders contain a list of data which will be deleted in 7 days
(`medium_priority_events`) and data which will be deleted in 3 days (`high_priority_events`). It is
your responsibility to download any data that you need before deletion.
This cron job should be run once per day:

```
# m h  dom mon dow   command
@daily /home/mysite/venv/bin/python /home/mysite/mysite/manage.py cleanup_and_remind
```


## Tests
The tests are written with
[pytest-django](https://pytest-django.readthedocs.io/en/latest/)
and can be run from the shell with `pytest` or `pytest --verbose` for more detailed output.


# License
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)


# Credits
EvR is maintained by @silverkszlo and @Therac-25.
Shoutout to @eli-quereli who initialized and realized this project together with us!

This project has been created for and with the [Educat Kollektiv - education from below](https://www.educat-kollektiv.org/).

[![educat logo](registration/static/registration/educat_logo_title_small.png)](https://www.educat-kollektiv.org/)

# Outlook
This project is being maintained and kept up to date. However we are currently not developing new features.
