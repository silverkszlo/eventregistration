# 1.4.8. (23.04.24)
- If in a WaitListEntryCreate form the participant name contains "://http" or "://https", no WLE will be
  created and no email is sent. This is an experiment that might help with the sign-up spam problem.
  This is pure security by obscurity, but depending on how much time the spammers invest, this might work

# 1.4.7 (17.01.24)
- Captchas now have an audio fallback option. Clicking the captcha will download a .wav file where the
  number in the captcha is read out loud. It doesn't have great usability, but it works. The title of the
  HTML element containing the captcha also describes how to do this.
  This makes the captchas more accessible for users who access the site through screenreaders.
  Not that `flite` needs to be installed under `/usr/bin/flite` (on the machine where EVR is running)
  in order for this to work. Install it with `sudo apt install flite`.

# 1.4.6 (21.09.23)
- Captchas are now used for WaitlistEntry creation whenever there have been more than 20 signups with
  unconfirmed email addresses in the last 24 hours. We assume that the vast majority of unconfirmed WLEs
  is spam. Also, we only want to use captchas when necessary, as they are terrible for both UX and
  accessibility. This will hopefully solve te current signup spam problem, at least until the attackers
  adapt their strategy.

# 1.4.5 (21.09.23)
- WaitListEntries can now be filtered for confirmed status, created at and reminded at.

# 1.4.4 (28.07.23)
- The wording of the event pricing fields has been slightly tweaked and a typo corrected

# 1.4.3 (18.07.23)
- Events can now have pricing fields for low, medium and average fees. While none of the fields are required,
  you have to fill out all of them if you fill out one of them. If they are present, users have to chose one
  of the values on creating a registration. Adapting, or even deleting the pricing categories later will not
  affect existing registrations.
- As the application now knows which events are paid and which not, the confirmation email that users receive
  after successfully registering for an event now contains the bank information for paid events and doesn't
  mention payment at all for free events.

# 1.4.1 (08.05.23)
- Updated dependencies

# 1.4.0 (26.03.23)

## Breaking changes
-   The URLs have been shortened. Also, events now use id's instead of UUID's as identifiers, since they look nicer and and there
    are no real benefits from the added security of UUID's because events are public anyway. Also, events use optional slugs now.
    "Optional" means that only the id is used to find the correct event, and the slug doesn't matter. All of this means that the
    event URLs change, so any URLs on other pages that point to events need to be adapted.
-   Event types (such as "trip aborad" or "hike") have been removed. This also means that the extra questions for full age and
    health insurance in the case of trips abroad do not exist anymore. If you want to ensure that participants fulfill extra
    requirements such as these, you should mention this in the event decription.

## New features
-   Participants can now sign up for a newsletter when signing up for the waitlist. The information about the newsletter request
    is automatically being sent to the email address from the ORGANIZER_CONTACT_EMAIL setting.
-   A custom admin action allows to send emails to selected participants via a mailto-link.
-   The event list now links directly to the waitlistentry create view, which has been modified to display the event details.
    This essentially saves one click between event detail view and waitlistentry create view.
-   An RSS feed with all events before the signup deadline is now available under /feed/


# 1.2.1. (24.09.22)

## Security
-   The content security policy was tightened to forbid `unsafe-inline` scripts, thus making XSS-attacks
    harder.


# 1.2.0 (08.09.22)

## New features
-   There is now the option to mark registration as being on the guestlist. Such registrations will
    not count when calculating the number of taken seats. Also people on the guestlist do not receive any kind of automated emails. In order to allow organizers to put people on the guestlist manually, the option to add registrations in the admin interface has been added.
    The mail program of the user is being used for this.
-   `Event.registration_deadline` has been split into `Event.signup_deadline` and
    `Event.signoff_deadline`. The time until registrations are possible and the time until free cancellation is possible can now be set independently of each other. The signoff-deadline has to come before the signup-deadline though.
