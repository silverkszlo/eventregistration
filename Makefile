VIRTUAL_ENV ?= venv
MANAGEPY = $(VIRTUAL_ENV)/bin/python manage.py

.PHONY: reset_database
reset_database: drop populate

.PHONY: drop
drop:
	@echo "Drop database"
	rm eventregistration/db.sqlite3
	python manage.py migrate
	@# repopulate the database with dummy data for development
	@echo "Create superuser"
	@echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@example.com', '123')" | python manage.py shell

.PHONY: run
run:
	pip install -r requirements/development.txt
	python manage.py runserver

.PHONY: lint
lint:
	$(VIRTUAL_ENV)/bin/isort registration tests
	$(VIRTUAL_ENV)/bin/flake8

.PHONY: makemessages
makemessages:
	export DJANGO_SETTINGS_MODULE=registrationform.settings.development
	$(MANAGEPY) makemessages --locale=de --extension py, --extension html --ignore=venv

.PHONY: populate
populate:
	@echo "Populate database"
	python manage.py create_demo_content

.PHONY: test
test:
	$(VIRTUAL_ENV)/bin/isort registration tests
	$(VIRTUAL_ENV)/bin/flake8
	pytest
	python manage.py makemigrations --check --dry-run
